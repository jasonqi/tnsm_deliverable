function output = adaptive_3_stable(time, util, sample_size,profile)
fileID = fopen('1log.txt','a');
fileID2 = fopen('2log.txt','a');
time = time-time(1);

% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);
% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);

lineFit1 = fit(time, util, 'pchipinterp');
time=[time(1):0.1:time(end)];
util=feval(lineFit1,time);
%plot(time,util)

% Initial monitoring period = 1
period = 1;


record_prediction = [];
record_period = [];
record_Y = [];
record_newY = [];
record_X = [];
record_decision = [];
record_R = [];
record_temp = [];

% Process the algorithm: Begin
% Fill in the sample queue: sample size = 10
queueX = [];
queueY = [];
size = sample_size;
dynamic_sample_size = size;

for index = 0:size-1
    queueX = [queueX,time(10*index+1)];
    queueY = [queueY,util(10*index+1)];
end
append_X = queueX;
append_Y = queueY;
% Queue is ready, carry on monitoring


while  index <= time(length(time))*0.98
% Compute prediction
prediction = 0;

for index_2 = 1:dynamic_sample_size -1 
    prediction = prediction + (queueY(index_2+1)-queueY(index_2))/(queueX(index_2+1)-queueX(index_2));
end
prediction = queueY(length(queueY))+prediction*(queueX(length(queueX))-queueX(length(queueX)-1))/(length(queueX)-1);

e = tsmovavg(queueY,'s',size-1);
e = e(length(e));

% Compute new measurement, move index forward
index = index + period;
index = round(index,1);
newX = time(10*index+1);
newY = util(10*index+1);

record_prediction = [record_prediction, prediction];
record_newY = [record_newY, newY];

% Compute ratio metric
if abs(prediction - queueY(length(queueY))) <=0.01
    numerator=0.01;
else 
    numerator = prediction - queueY(length(queueY));
end

if abs(newY - queueY(length(queueY))) <= 0.01
    denominator = 0.01;
else
    denominator = newY - queueY(length(queueY));
end

R = abs(numerator/denominator);

if abs(prediction - queueY(length(queueY))) <= 0.15
    R = 1;
end

if R >= 0
    period = min([10*R,period+period*R]);
    dynamic_sample_size = dynamic_sample_size + 1;
    if dynamic_sample_size >= size
        dynamic_sample_size = size;
    end
else
    period = period-period*abs(R);
    dynamic_sample_size = round(dynamic_sample_size / 4);
    if dynamic_sample_size <= 2
        dynamic_sample_size = 2;
    end
end
period = period / 8;
if period > 6
    period = 6;
end
if period < 0.1
    period = 0.1;
end

% +++++++Choose sample size++++++++
% if abs(newY - queueY(length(queueY)))/queueY(length(queueY)) > 0.25
%     dynamic_sample_size = dynamic_sample_size + 1;
%     if dynamic_sample_size >= size
%         dynamic_sample_size = size;
%     end
% else
%     dynamic_sample_size = round(dynamic_sample_size / 2);
%     if dynamic_sample_size <= 2
%         dynamic_sample_size = 2;
%     end
% end

% Record log 

record_Y = [record_Y, queueY(length(queueY))];

record_X = [record_X, queueX(length(queueX))];
%record_R = [record_R, R];

% Update the queues
queueX = queueX(2:end);
queueY = queueY(2:end);
queueX(end+1) = newX;
queueY(end+1) = newY;

end

% Process new plots

%hold on;
x2 = [append_X(1:end-1), record_X];
y2 = [append_Y(1:end-1), record_Y];
x2=transpose(x2);
y2=transpose(y2);
lineFit2 = fit(x2, y2, 'nearestinterp');
y2=feval(lineFit2,time);
%plot (time,y2);

Error = sqrt(mean((util - y2).^2));
fprintf(fileID, 'Algorithm=3 Max/Initial_Sample_Size=%d RMSE=%f msgs=%d profile=%d\n',sample_size,Error,length(x2),profile);
for index_3 = 1:length(record_period)-1
    fprintf(fileID2, 'Prediction=%f newY=%f dev=%f Frequency=%f, Period=%f\n',record_prediction(index_3),record_newY(index_3),record_temp(index_3),1/record_period(index_3),record_period(index_3));
end

%grid on;
%legend('Ground Truth','Adaptive Scheme3');
%ylabel('Link Util / Mb');
%xlabel('Time / s');
%ylim([4 11]);
%size = num2str(size,3);
%size = strcat('Sample Size = ', size);
%title({'adaptive_3',size,'RMS Error:',Error,'Msg:',length(x2)})
output = 0;
