
function output = adaptive_2(time, util, upper_range, lower_range, sample_size, profile)
fileID = fopen('log.txt','a');
time = time-time(1);
xn = time;
yn = util;
% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);
% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);

lineFit1 = fit(time, util, 'pchipinterp');
time=[time(1):0.1:time(end)];
util=feval(lineFit1,time);
%plot(time,util)

% Initial monitoring period = 1
period = 1;

% Upper and lower bounds for ratio metric R
upper = upper_range;
lower = lower_range;

record_prediction = [];
record_period = [];
record_Y = [];
record_newY = [];
record_X = [];
record_decision = [];
record_R = [];

% Process the algorithm: Begin
% Fill in the sample queue: sample size = 10
queueX = [];
queueY = [];
size = sample_size;
for index = 0:size - 1
    queueX = [queueX,time(10*index+1)];
    queueY = [queueY,util(10*index+1)];
end
append_X = queueX;
append_Y = queueY;
% Queue is ready, start monitoring

while  index <= time(length(time))*0.98
% Compute prediction
prediction = 0;
for index_2 = 1:length(queueX)-1
    prediction = prediction + (queueY(index_2+1)-queueY(index_2))/(queueX(index_2+1)-queueX(index_2));
end
prediction = queueY(length(queueY))+prediction*(queueX(length(queueX))-queueX(length(queueX)-1))/(length(queueX)-1);


% Compute new measurement, move index forward
index = index + period;
index = round(index,1);
newX = time(10*index+1);
newY = util(10*index+1);



% Compute ratio metric
if abs(prediction - queueY(length(queueY))) <=0.01
    numerator=0.01;
else 
    numerator = abs(prediction - queueY(length(queueY)));
end

if abs(newY - queueY(length(queueY))) <= 0.01
    denominator = 0.01;
else
    denominator = abs(newY - queueY(length(queueY)));
end

R = abs(numerator/denominator);

if abs(prediction - queueY(length(queueY))) <= 0.15
    R = 1;
end

record_period = [record_period, period];

% Determine new monitoring period
if R >= upper
    % Reduce frequency
     record_decision = [record_decision, 'down '];
    period = period * 2;
    if period > 10
        period = 10;
    end
elseif R <= lower
    % Increase frequency
    record_decision = [record_decision, 'up '];
    period = period / 2;
    if period <0.1
        period = 0.1;
    end   
else 
   record_decision = [record_decision, 'unchanged '];
end

% if queueY(length(queueY)) == newY
%     period = 0.1;
% end  
% 
% if abs(prediction - queueY(length(queueY))) <= 0.05
%     period = 0.1;
% end

% Record log 
record_prediction = [record_prediction, prediction];
record_Y = [record_Y, queueY(length(queueY))];
record_newY = [record_newY, newY];
record_X = [record_X, queueX(length(queueX))];
record_R = [record_R, R];

% Update the queues
queueX = queueX(2:end);
queueY = queueY(2:end);
queueX(end+1) = newX;
queueY(end+1) = newY;

end

% Process new plots
%hold on;
x2 = [append_X(1:end-1), record_X];
y2 = [append_Y(1:end-1), record_Y];
x2=transpose(x2);
y2=transpose(y2);
lineFit2 = fit(x2, y2, 'nearestinterp');
y2=feval(lineFit2,time);
%plot (time,y2);

Error = sqrt(mean((util - y2).^2));
fprintf(fileID, 'Algorithm=2 upper_range=%.2f lower_range=%.2f sample_size=%d RMSE=%f msgs=%d profile=%d\n',upper_range,lower_range,sample_size,Error,length(x2),profile);

%grid on;
%legend('Ground Truth','Adaptive Scheme2');
%ylabel('Link Util / Mb');
%xlabel('Time / s');
%ylim([4 11]);
%size = num2str(size,3);
%size = strcat('Sample Size = ', size);
%upper = num2str(upper,3);
%lower = num2str(lower,3);
%ranges = strcat('Upper = ',upper, ' Lower = ', lower);
%title({'adaptive_2',size,ranges,'Multiplication Constant = 2 Division Constant = 2','RMS Error:',Error,'Msg:',length(x2)})
output = 0;
end
