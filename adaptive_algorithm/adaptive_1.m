% 
% function output = adaptive_1(time, util, multiplier, divider, threshold, profile, threshold_low)
% fileID = fopen('log.txt','a');
% time = time-time(1);
% xn = time;
% yn = util;
% % time = vertcat(time, time(length(time))+time+0.01);
% % util = vertcat(util, util);
% % time = vertcat(time, time(length(time))+time+0.01);
% % util = vertcat(util, util);
% 
% lineFit1 = fit(time, util, 'pchipinterp');
% time=[time(1):0.1:time(end)];
% util=feval(lineFit1,time);
% % plot(time,util)
% 
% % Initial monitoring period = 1
% period = 1;
% 
% mul_con = multiplier;
% div_con = divider;
% 
% record_period = [];
% record_Y = [];
% record_newY = [];
% record_X = [];
% record_decision = [];
% 
% % Process the algorithm: Begin
% 
% % Queue is ready, start monitoring
% index = 0;
% while  index <= time(length(time))*0.98
% % Compute prediction
% 
% oldY = util(10*index+1);
% % Compute new measurement, move index forward
% index = index + period;
% index = round(index,1);
% newX = time(10*index+1);
% newY = util(10*index+1);
% 
% difference = abs(newY- oldY)/newY;
% 
% if newY == oldY && newY ==0
%     difference = threshold-0.01;
% end    
% if newY == oldY
%     difference = threshold-0.01;
% end 
% 
% record_period = [record_period, period];
% 
% % Determine new monitoring period
% if difference < threshold_low
%     % Reduce frequency
%      record_decision = [record_decision, 'down '];
%     period = period * mul_con;
%     if period > 10
%         period = 10;
%     end
% elseif difference > threshold
%     % Increase frequency
%     record_decision = [record_decision, 'up '];
%     period = period / div_con;
%     if period <0.1
%         period = 0.1;
%     end    
% end
% 
% % Record log 
% record_newY = [record_newY, newY];
% record_X = [record_X, newX];
% 
% 
% end
% 
% % Process new plots
% % hold on;
% x2 = record_X;
% y2 = record_newY;
% x2=transpose(x2);
% y2=transpose(y2);
% lineFit2 = fit(x2, y2, 'nearestinterp');
% y2=feval(lineFit2,time);
% % plot (time,y2);
% 
% Error = sqrt(mean((util - y2).^2));
% fprintf(fileID, 'Algorithm=1 m=%d d=%d th_high=%f th_low=%f RMSE=%f msgs=%d profile=%d\n',multiplier,divider,threshold,threshold_low,Error,length(x2),profile);
% % grid on;
% % legend('Ground Truth','Adaptive Scheme1');
% % ylabel('Link Util / Mb');
% % xlabel('Time / s');
% % ylim([4 11]);
% % mul_con = num2str(mul_con,2);
% % div_con = num2str(div_con,2);
% % con = strcat('Multiplier = ',mul_con,' Divider = ',div_con);
% % threshold = threshold*100;
% % threshold = num2str(threshold,2);
% % th = strcat('Threshold = ', threshold,'%');
% % title({'adaptive_1', con, th, 'RMS Error:',Error,'Msg:',length(x2)})
% output = 0;
% end

function output = adaptive_1(time, util, multiplier, divider, threshold, profile)
fileID = fopen('log.txt','a');
time = time-time(1);
xn = time;
yn = util;
% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);
% time = vertcat(time, time(length(time))+time+0.01);
% util = vertcat(util, util);

lineFit1 = fit(time, util, 'pchipinterp');
time=[time(1):0.1:time(end)];
util=feval(lineFit1,time);
% plot(time,util)

% Initial monitoring period = 1
period = 1;

mul_con = multiplier;
div_con = divider;

record_period = [];
record_Y = [];
record_newY = [];
record_X = [];
record_decision = [];

% Process the algorithm: Begin

% Queue is ready, start monitoring
index = 0;
while  index <= time(length(time))*0.98
% Compute prediction

oldY = util(10*index+1);
% Compute new measurement, move index forward
index = index + period;
index = round(index,1);
newX = time(10*index+1);
newY = util(10*index+1);

difference = abs(newY- oldY)/newY;

if newY == oldY && newY ==0
    difference = threshold-0.01;
end    
if newY == oldY
    difference = threshold-0.01;
end 

record_period = [record_period, period];

% Determine new monitoring period
if difference < threshold
    % Reduce frequency
     record_decision = [record_decision, 'down '];
    period = period * mul_con;
    if period > 10
        period = 10;
    end
else
    % Increase frequency
    record_decision = [record_decision, 'up '];
    period = period / div_con;
    if period <0.1
        period = 0.1;
    end   
end

% Record log 
record_newY = [record_newY, newY];
record_X = [record_X, newX];


end

% Process new plots
% hold on;
x2 = record_X;
y2 = record_newY;
x2=transpose(x2);
y2=transpose(y2);
lineFit2 = fit(x2, y2, 'nearestinterp');
y2=feval(lineFit2,time);
% plot (time,y2);

Error = sqrt(mean((util - y2).^2));
fprintf(fileID, 'Algorithm=1 m=%d d=%d th=%f RMSE=%f msgs=%d profile=%d\n',multiplier,divider,threshold,Error,length(x2),profile);
% grid on;
% legend('Ground Truth','Adaptive Scheme1');
% ylabel('Link Util / Mb');
% xlabel('Time / s');
% ylim([4 11]);
% mul_con = num2str(mul_con,2);
% div_con = num2str(div_con,2);
% con = strcat('Multiplier = ',mul_con,' Divider = ',div_con);
% threshold = threshold*100;
% threshold = num2str(threshold,2);
% th = strcat('Threshold = ', threshold,'%');
% title({'adaptive_1', con, th, 'RMS Error:',Error,'Msg:',length(x2)})
output = 0;
end
