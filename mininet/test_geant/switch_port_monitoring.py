#!/usr/bin/python

import re
import os
import sys
import math
import subprocess
import time
from datetime import datetime
from time import sleep
from optparse import OptionParser

if __name__ == '__main__':
	
	# parse options for the link number
	parser = OptionParser()
        parser.add_option("-l", "--link", dest="link", help="link id to be monitored, without the prefix 'L'")
	(options, args) = parser.parse_args()
	if options.link == None:
                sys.exit("Error: Not all options are set! Use -h, for help")

	# find the two switches connected to this link
	f = open("Test1_output/out_links_if.txt", "r")
	topo = f.read().split()
	f.close()
	sleep(4.5)
	# dump the switche and find the relevant port; then grep the corresponding number to calculate link utilisation
	cmd = "sudo ovs-ofctl dump-ports s"+topo[topo.index('L'+options.link)+3][1:]+" | egrep -A 1 'port  "+topo[topo.index('L'+options.link)+4][3:]+":' | grep -o '[0-9]*'"

	f1= open("util.log","w+")
	f2= open("time.log","w+")
	output_previous=0
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	print 'Monitoring Link '+options.link+', press "ctl+c" to terminate...'
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	while True:
		ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
		output = ps.communicate()[0]
		output = max(map(int, output.split()))
		result = (output-output_previous)*0.00001
		if result >= 20:
			continue
		#f1.write(str((output-output_previous)*0.00001)+'\n')
		f1.write(str(result)+'\n')
		print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		print result
		output_previous=output
		f2.write(str(time.time())+'\n')
		sleep (0.8)
