import re
import sys
from optparse import OptionParser
import networkx as nx
import collections


def overlapping_degree(A_list, source, sink ):  #FIXME This is the optimal way, I can only count the shared links to speed up the operation

	#Initialize overlapping matrices:
	#	overlapping_c[i][j] = number of links in i contained also in j
	#	norm_overlapping_c[i][j] = (number of links in i contained also in j)(total number of links in i)
	overlapping_c = [[0 for x in range(len(A_list))] for x in range(len(A_list))]
	norm_overlapping_c = [[0.0 for x in range(len(A_list))] for x in range(len(A_list))]

	for i in range(len(A_list)):
		for j in range(len(A_list)):
			if i!=j:
				for k in range(len(A_list[i])-1):
					for h in range(len(A_list[j])-1):
						if A_list[i][k] == A_list[j][h] and A_list[i][k+1] == A_list[j][h+1]:
							overlapping_c[i][j]+=1
				norm_overlapping_c[i][j] = float(overlapping_c[i][j]) / (len(A_list[i])-1)

	print "Overlapping matrix: " + str(overlapping_c)
	print "Normalized overlapping matrix (" + source + "," + sink + ") : " + str(norm_overlapping_c)

	#Compute the overlapping coefficient
	overlap = [0 for x in range(len(A_list))]
	print len(A_list)
	for i in range(len(A_list)):
		for j in range(len(A_list)):
			overlap[i] += norm_overlapping_c[i][j]
		overlap[i] = overlap[i] / (len(A_list)-1) #nullify the impact of diagonal elements

	print "Shortest path length : " + str(len(A_list[0])) + "  Overlapping coefficient (" + source + "," + sink + ") : " + str( sum(overlap) / (len(overlap)) )

	#Return the overlapping coefficient
	return sum(overlap)/len(overlap)


def save_on_file(A_list, source, sink, directory):
	#open specific output file
	try:
		f_out = open(directory + "/sp_output_"+source+"_"+sink, 'w')
	except:
		sys.exit("Error: While opening the output file")
		return 1
	#open general path file (append)
	try:
		f_gen = open(directory + "/sp_output.txt", 'a')
	except:
		sys.exit("Error: While opening the output file")
		return 1
	for i in range(len(A_list)):
		f_out.write(A_list[i][0])
		f_gen.write(A_list[i][0])
		for j in range(1, len(A_list[i])):
			f_out.write(" " + A_list[i][j])
			f_gen.write(" " + A_list[i][j])
		f_out.write("\n")
		f_gen.write("\n")
	f_out.close()
	f_gen.close()

def clean_path_list(A_list, source, sink):
	print "Exceeding overlapping threshold"
	#TODO Do something here...

def get_B_weight(B_list):
	return B_list[1]

def yens_algo(topo_file, npaths, source, dest, verbose = False):
	#Build the networkx graph from file
	G=nx.read_edgelist(topo_file, nodetype=str)

	##K-shortest paths by the Yen's Algorithm (following wikipedia formulation)##

	#Initialize the required list
	A=[]
	B=[]
	removed_edges = []
	removed_nodes = []

	#Parse command line arguments
	K= npaths #K-shortest paths
	SOURCE = source #(string)
	SINK = dest #(string)
	if verbose == True:
		print "Average shortest path length: " + str(nx.average_shortest_path_length(G))

	##Compute the Dijkstra shortest path and append to list A
	A.append(nx.dijkstra_path(G,SOURCE,SINK))
	#print nx.shortest_path_length(G, SOURCE, SINK)

	for k in range (1,K):
		for i in range ( 0, len(A[k-1])-1 ):
			spur_n = A[k-1][i]
			root_p = A[k-1][0:i+1] #mod index
			if verbose == True:
				print "\n\nSpur node :"
				print spur_n
				print "Root path :"
				print root_p
			for path in A:
				if root_p == path[0:i+1]: #mod index
					edge_to_remove =  path[i], path[i+1]
					if verbose == True:
						print "Edge to be remove (if not done for another path)"
						print edge_to_remove

					#save the weight and append to the list of removed edges (if not already removed!)
					already_removed = False
					for e in removed_edges:
						if e[0]==edge_to_remove[0] and e[1]==edge_to_remove[1]:
							already_removed = True
					if already_removed == False:
						removed_edges.append([edge_to_remove[0], edge_to_remove[1]])
						if verbose == True:
							print "Removed edge:"
							print edge_to_remove
						G.remove_edge(edge_to_remove[0], edge_to_remove[1])

			for root_p_node in root_p[0:i]:
				#First remove all the edges involving the selected node
				for edge in G.edges():
					if edge[0] == root_p_node or edge[1] == root_p_node:
						removed_edges.append([edge[0], edge[1]])
						G.remove_edge(edge[0], edge[1])

				#Then remove the nodes
				G.remove_node(root_p_node)
				removed_nodes.append(root_p_node)
				if verbose == True:
					print "Removed nodes:"
					print removed_nodes

			#Find the spur-sink shortest path (if any path exists)
			found_sh_path = False
			try:
				spur_p = nx.dijkstra_path(G,spur_n,SINK)
				total_p = root_p + spur_p[1:]
				if verbose == True:
					print "Total path :"
					print total_p
				found_sh_path = True
			except:
				if verbose == True:
					print "No path from spur to sink. Go on..."

			#Restore the graph
			for node in removed_nodes:
				G.add_node(node)
			del removed_nodes[:]

			for edge in removed_edges:
				G.add_edge(edge[0], edge[1])
			del removed_edges[:]

			#If a path from spur node to the sink has been found, add to B list
			if found_sh_path == True:
				#compute the cost of the new path (total_p)
				cost = 0
				for i in range(0, len(total_p)-1):
					cost+= 1 #we only consider the hop count
				if verbose == True:
					print cost
				#append the new path to B
				if [total_p, cost] not in B:
					B.append([total_p, cost]) #FIXME Better to create a "stack" class for B
					if verbose == True:
						print "B:"
						print B
				else:
					if verbose == True:
						print "Path is already in stack B"

		if len(B) == 0:
			if verbose == True:
				print "No path avaiable. Ending..."
			break

		B.sort(key=get_B_weight)
		if verbose == True:
			print "\nB:"
			print B

		#Append the path to the list of the shortest paths
		A.append(B[0][0])
		if verbose == True:
			print "A:"
			print A

		#Finally pop(B)
		B.pop(0)
		if verbose == True:
			print "B, after pop(B):"
			print B

	if verbose == True:
		print "\n\nResult:"
		print A
	return A
