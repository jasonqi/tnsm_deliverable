#/usr/bin/python

import re
import os
import sys
from datetime import datetime
from multiprocessing import Process
from time import sleep
from optparse import OptionParser

DEFAULT_IP = "10.0.0.9"
DEFAULT_PORT = 8080
N_WEBSERV = 1
DEFAULT_FNAME = "hello.txt"
MAX_PROC = 20


def http_req(server_ip, server_port, file_name, client):
    f_out  = open(client + "_http_output.txt", 'a')
    f_out.write("client " + client + " " + server_ip + ':' + str(server_port) + ": " + str(datetime.now().time()) + ' : ')
    f_out.write("curl -w @curl-format.txt -o /dev/null -s http://" + server_ip + ':' + str(server_port) + '\n\n')
    f_out.close()
    os.system("curl -w @curl-format.txt -o /dev/null -s http://" + server_ip + ':' + str(server_port) + '/' + file_name + " 1>>" + client + "_http_output.txt --retry 0 &")
    os.system("rm -f hello.txt.* &")

if __name__ == '__main__':
    proc_list = []
    parser = OptionParser()
    parser.add_option("-i", "--inputtrace", dest="tracefile", help="Input trace file")
    parser.add_option("-c", "--client", dest="client", help="Id of client")
    (options, args) = parser.parse_args()
    if options.tracefile== None or options.client == None:
            sys.exit("Error: Not all options are set! Use -h, for help")
    
    if options.client=='1':
    	    os.system("rm -f http_output.txt")
    	    os.system("rm -f out.txt")
    try:
            f_trace = open(options.tracefile, 'r')
    except:
            sys.exit("Error while opening trace file")
    curr_tstamp = float(f_trace.readline().split(',')[0])
    f_trace.seek(0,0)
    n_req = 0
    for line in f_trace:
            new_tstamp = float(line.split(',')[0])
            sleep_time = new_tstamp - curr_tstamp
            curr_tstamp = new_tstamp
	    sleep(sleep_time)
	    #open client mapping file
            server_ip = None
            server_port = None
            f_mapping = open('/home/mininet/pox/config/mapping.txt', 'r')
            for line in f_mapping:
                line = line[:len(line)-1]
                line = line.split(" ")
                if (line[0].split(':'))[0] == options.client:
                        server_ip = (line[0].split(':'))[1]
                        server_port = DEFAULT_PORT
                        break
            f_mapping.close()
            print server_ip + ' ' + str(server_port)
            http_req(server_ip, server_port+(n_req%N_WEBSERV), DEFAULT_FNAME, options.client)
            #proc_list.append(Process(target=http_req, args=(DEFAULT_IP, DEFAULT_PORT+(n_req%N_WEBSERV), DEFAULT_FNAME)))
            #proc_list[len(proc_list)-1].start()
	    #if len(proc_list)> MAX_PROC:
	    #	proc_list.pop(0).join
	    n_req+=1
