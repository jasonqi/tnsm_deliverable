#!/usr/bin/python
import re
import os
import sys
from optparse import OptionParser

if __name__ == '__main__':
	f_out = {}
	parser = OptionParser()
        parser.add_option("-i", "--inputtrace", dest="tracefile", help="Input trace file")
    	parser.add_option("-n", "--nclients", dest="nclients", help="number of clients")
	(options, args) = parser.parse_args()
	if options.tracefile== None or options.nclients == None:
		sys.exit("Error: Not all options are set! Use -h, for help")
	try:
        	f_trace = open(options.tracefile, 'r')
    	except:
        	sys.exit("Error while opening trace file")
	for i in range(0,int(options.nclients)):
		f_out[i+1] = open(str(i+1) + '_' + options.tracefile, 'w')
	j = 0
	for line in f_trace:
		k = (j % int(options.nclients)) + 1
		f_out[k].write(line)
		j+=1		
