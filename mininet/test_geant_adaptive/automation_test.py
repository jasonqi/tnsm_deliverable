#!/usr/bin/python

import re
import os
import sys
import math
from time import sleep
from optparse import OptionParser
import subprocess
import signal


def file_len(fname):
    with open(fname) as f_func:
        for i, l in enumerate(f_func):
            pass
    return i + 1

if __name__ == '__main__':

	wd = "/home/mininet/test_geant_adaptive/"	
	#links = ['L25','L26','L30']
	#links = ['L25']
	#targets = ['s10:eth3','s10:eth4','s3:eth5']
	#mgr = ['16','10','11']
	links = ['L14']
	targets = ['s1:eth3']
	mgr = ['1']
	concatenation = [1,2,3]
	def timeout_handler(signum, frame):
    		raise Exception("timeout")

	signal.signal(signal.SIGALRM, timeout_handler)
	
	os.chdir("/home/mininet/pox")
	subprocess.call('./Stop_all_Local_Managers.sh', shell=True)
	subprocess.call("kill -9 `ps aux |grep switch_port_monitoring.py |awk '{print $2}'`", shell = True)
	counter = 0
	counter2 = 0
	for link in links:

                # Modify const.py
                f_const = open('/home/mininet/pox/ext/const.py','r')
                lines = f_const.readlines()
                lines[9-1]="CONG_LINK = '"+link+"' #only consider this link when doing offloading (HACK!)\n"
                f_const.close()
                f_const = open('/home/mininet/pox/ext/const.py','w')
                f_const.writelines(lines)
                f_const.close()

                # Modify App_cong_det.py
                f_cong = open('/home/mininet/pox/ext/App_cong_det.py','r')
                lines = f_cong.readlines()
                lines[111-1]="                        if link_id == '"+str(link)+"':\n"
                f_cong.close()
                f_cong = open('/home/mininet/pox/ext/App_cong_det.py','w')
                f_cong.writelines(lines)
                f_cong.close()
		
		# Modify plot title
		f_log = open(wd+'/concatenation_files/title', 'w')
                f_log.write("title({'Latency & Link Utilisation','Geant Topology','5LM Adaptive', 'Link "+link[1:]+"','Recovery Time / seconds',width});")
                f_log.close()

		
		# Modify Adaptive algorithm 
		f_ada = open('/home/mininet/pox/ext/Mon_mod_res_processor_adaptive.py','r')
		lines = f_ada.readlines()
		lines[46-1]="            if str(low_level_target) != '"+str(targets[counter2])+"':\n"
		lines[43-1]="            if self.mgr != "+str(mgr[counter2])+":\n"
		f_ada.close()
		f_ada = open('/home/mininet/pox/ext/Mon_mod_res_processor_adaptive.py','w')
		f_ada.writelines(lines)
		f_ada.close()
		
		'''
		# Modify config/cd.txt
		f_cd = open('/home/mininet/pox/config/cd.txt','r')
		lines = f_cd.readlines()
		lines[1-1]="CD_1 L14,"+str(link)+",L12 1\n"	
		f_cd.close()
		f_cd = open('/home/mininet/pox/config/cd.txt','w')
		f_cd.writelines(lines)
		f_cd.close()
		'''

		for part in concatenation:
			# Modify cd.txt for monitored link, TODO: make sure links can be congested, precisely
			#f = open('/home/mininet/pox/cd.txt')
			# Do something, maybe not needed
			#f.close()
			
			# Start experiment
			os.chdir("/home/mininet/pox")
			subprocess.call('rm util_pox.log',shell=True)
			subprocess.call('rm time_pox.log',shell=True)
			subprocess.call('./adaptive_Local_Manager.sh 1 &', shell=True)
			subprocess.call('./adaptive_No_Path_Local_Manager.sh 16 &', shell=True)
			subprocess.call('./adaptive_No_Path_Local_Manager.sh 10 &', shell=True)
			subprocess.call('./adaptive_No_Path_Local_Manager.sh 11 &', shell=True)
			subprocess.call('./adaptive_No_Path_Local_Manager.sh 3 &', shell=True)
			os.chdir(wd)
			sleep(4)
			subprocess.call('python switch_port_monitoring.py -l '+link[1:]+' &', shell=True)
			signal.alarm(35)
			try:
				subprocess.call('sudo python test1http.py -i Test1_input -o Test1_output -d 0 -p test1.txt ', shell=True)

			except Exception as exc:
				# Terminate experiment, clean up
				signal.alarm(0)
				subprocess.call('sudo mn -c', shell=True)
				subprocess.call('killall -9 python2.7', shell=True)
				subprocess.call('killall -9 sh', shell=True)
				subprocess.call("kill -9 `ps aux |grep switch_port_monitoring.py |awk '{print $2}'`", shell = True)
				os.chdir("/home/mininet/pox")
				subprocess.call('./Stop_all_Local_Managers.sh', shell=True)

				# Generate log: lat.log, util.log, time.log, 5_client_lat.log, 5_clientl_time.log, msg exchanged???
				# msg exchanged: 
				num = file_len('/home/mininet/pox/util_pox.log')
				f_num = open(wd+'temp/msg.txt','a')
				f_num.write(str(num)+'\n')
				f_num.close()
				# 5_client_lat.log
				subprocess.call('cat '+wd+'5_client_lat.log > '+wd+'temp/client_lat_'+str(part)+'', shell = True)
				# 5_client_time.log
				subprocess.call('cat '+wd+'5_client_time.log > '+wd+'/temp/client_time_'+str(part)+'', shell = True)
				# util.log
			    	subprocess.call('cat '+wd+'util.log > '+wd+'temp/util_'+str(part)+'', shell = True)	
				# time.log
				subprocess.call('cat '+wd+'time.log > '+wd+'temp/time_'+str(part)+'', shell = True)
				# lat.log
				subprocess.call('cat /home/mininet/pox/lat.log > '+wd+'temp/lat.log_'+str(part)+'', shell = True)
				
				subprocess.call('cat '+wd+'5_http_output.txt > '+wd+'temp/1_http_output_'+str(part)+'.txt', shell = True)	 
				# rename: check
				# mv to temp folder: check

		# go to temp folder,
		os.chdir(wd+'temp')	
		# concatenation
		subprocess.call('cp ../concatenation_files/* .', shell = True)
		subprocess.call('cat client_lat_1 client_lat_2 client_lat_3 > client_latency.txt', shell = True)
		subprocess.call('cat client_time_1 client_time_2 client_time_3 > client_time.txt', shell = True)
		subprocess.call('cat util_1 util_2 util_3 > link_util.txt', shell = True)
		subprocess.call('cat time_1 time_2 time_3 > link_time.txt', shell = True)
		subprocess.call('cat lat.log_1 lat.log_2 lat.log_3 > lat.log', shell = True)
		
		# append matlab script format, make addition
		subprocess.call('cat ctime1 client_time_1 close ctime2 client_time_2 close ctime3 client_time_3 close clat1 client_lat_1 close clat2 client_lat_2 close clat3 client_lat_3 close util1 util_1 close util2 util_2 close util3 util_3 close time1 time_1 close time2 time_2 close time3 time_3 close matlab_tail title> matlab_plot.txt', shell = True)			
		# move to official folder
		os.chdir(wd)
		subprocess.call('mkdir '+wd+'adaptive/'+link+'/', shell = True)
		os.chdir(wd+'temp')
		subprocess.call('mv matlab_plot.txt client_latency.txt client_time.txt link_util.txt link_time.txt lat.log msg.txt 1_http_output* '+wd+'adaptive/'+link+'/', shell = True)

		# make sure temp is empty
		subprocess.call('rm '+wd+'temp/*', shell = True)
		counter2 = counter2+1

