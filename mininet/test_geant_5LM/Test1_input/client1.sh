#!/bin/bash

echo "Start client request trace"
rm 1_http_output.txt
#sleep 8
sleep 0.02
#./ultra_ping/echo.py --client 10.0.0.11 --output_filename 1_client_lat.log --output_time 1_client_time.log &
python run_request_trace.py -i ./req_traces/1_rdmdeltacom_high_load_short.txt -c 1 &
# sin traffic for interval=[10ms,100ms,1s], period=1s
#python generate_sin_iperf.py -i 0.01 -p 1 -d 10.0.0.16
#python generate_sin_iperf.py -i 0.1 -p 1 -d 10.0.0.18
#python generate_sin_iperf.py -i 1 -p 1 -d 10.0.0.10

# sin traffic for interval=[10ms,100ms,1s], period=10s
#python generate_sin_iperf.py -i 0.01 -p 10 -d 10.0.0.16
#python generate_sin_iperf.py -i 0.1 -p 10 -d 10.0.0.18
#python generate_sin_iperf.py -i 1 -p 10 -d 10.0.0.14

# sin traffic for interval=[10ms,100ms,1s], period=100s
#python generate_sin_iperf.py -i 0.01 -p 100 -d 10.0.0.13
#python generate_sin_iperf.py -i 0.1 -p 100 -d 10.0.0.12
#python generate_sin_iperf.py -i 1 -p 100 -d 10.0.0.15

# start Poisson burst traffic
#python generate_burst_iperf.py -d 10.0.0.15 &
