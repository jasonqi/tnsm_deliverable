#!/usr/bin/python

import re
import os
import sys
import math
from time import sleep
from optparse import OptionParser
import subprocess
import signal


def file_len(fname):
    with open(fname) as f_func:
        for i, l in enumerate(f_func):
            pass
    return i + 1

if __name__ == '__main__':

	wd = "/home/mininet/test_geant_5LM/"	
	links = ['L14','L25','L26','L30']
	#links = ['L30']
	#links = ['L14']
	#targets = ['s1:eth3']
	mgr = ['1']
	targets = ['s1:eth3','s10:eth3','s10:eth4','s3:eth5']
	concatenation = [1,2,3]
	def timeout_handler(signum, frame):
    		raise Exception("timeout")

	signal.signal(signal.SIGALRM, timeout_handler)
	
	os.chdir("/home/mininet/pox")
	subprocess.call('./Stop_all_Local_Managers.sh', shell=True)
	subprocess.call("kill -9 `ps aux |grep switch_port_monitoring.py |awk '{print $2}'`", shell = True)
	counter = 0
	counter2 = 0
	for link in links:

                # Modify const.py
                f_const = open('/home/mininet/pox/ext/const.py','r')
                lines = f_const.readlines()
                lines[9-1]="CONG_LINK = '"+link+"' #only consider this link when doing offloading (HACK!)\n"
                f_const.close()
                f_const = open('/home/mininet/pox/ext/const.py','w')
                f_const.writelines(lines)
                f_const.close()

                # Modify App_cong_det.py
                f_cong = open('/home/mininet/pox/ext/App_cong_det.py','r')
                lines = f_cong.readlines()
                lines[111-1]="                        if link_id == '"+str(link)+"':\n"
                f_cong.close()
                f_cong = open('/home/mininet/pox/ext/App_cong_det.py','w')
                f_cong.writelines(lines)
                f_cong.close()
		
		# Modify plot title
		f_log = open(wd+'/concatenation_files/title', 'w')
                f_log.write("title({'Latency & Link Utilisation','Geant Topology','5LM 10Hz', 'Link "+link[1:]+"','Recovery Time / seconds',width});")
                f_log.close()

		for part in concatenation:
			# Modify cd.txt for monitored link, TODO: make sure links can be congested, precisely
			#f = open('/home/mininet/pox/cd.txt')
			# Do something, maybe not needed
			#f.close()
			
			# Start experiment
			os.chdir("/home/mininet/pox")
			subprocess.call('rm util_pox.log',shell=True)
			subprocess.call('rm time_pox.log',shell=True)
			subprocess.call('./Local_Manager.sh 1 &', shell=True)
			subprocess.call('./Local_Manager_no_LB.sh 16 &', shell=True)
			subprocess.call('./Local_Manager_no_LB.sh 10 &', shell=True)
			subprocess.call('./Local_Manager_no_LB.sh 11 &', shell=True)
			subprocess.call('./Local_Manager_no_LB.sh 3 &', shell=True)
			os.chdir(wd)
			sleep(4)
			subprocess.call('python switch_port_monitoring.py -l '+link[1:]+' &', shell=True)
			signal.alarm(35)
			try:
				subprocess.call('sudo python test1http.py -i Test1_input -o Test1_output -d 0 -p test1.txt ', shell=True)

			except Exception as exc:
				# Terminate experiment, clean up
				signal.alarm(0)
				subprocess.call('sudo mn -c', shell=True)
				subprocess.call('killall -9 python2.7', shell=True)
				subprocess.call('killall -9 sh', shell=True)
				subprocess.call("kill -9 `ps aux |grep switch_port_monitoring.py |awk '{print $2}'`", shell = True)
				os.chdir("/home/mininet/pox")
				subprocess.call('./Stop_all_Local_Managers.sh', shell=True)

				# Generate log: lat.log, util.log, time.log, 5_client_lat.log, 5_clientl_time.log, msg exchanged???
				# msg exchanged: 
				num = file_len('/home/mininet/pox/util_pox.log')
				f_num = open(wd+'temp/msg.txt','a')
				f_num.write(str(num)+'\n')
				f_num.close()
				# 5_client_lat.log
				subprocess.call('cat '+wd+'5_client_lat.log > '+wd+'temp/client_lat_'+str(part)+'', shell = True)
				# 5_client_time.log
				subprocess.call('cat '+wd+'5_client_time.log > '+wd+'/temp/client_time_'+str(part)+'', shell = True)
				# util.log
			    	subprocess.call('cat '+wd+'util.log > '+wd+'temp/util_'+str(part)+'', shell = True)	
				# time.log
				subprocess.call('cat '+wd+'time.log > '+wd+'temp/time_'+str(part)+'', shell = True)
				# lat.log
				subprocess.call('cat /home/mininet/pox/lat.log > '+wd+'temp/lat.log_'+str(part)+'', shell = True)
					 
				# rename: check
				# mv to temp folder: check

		# go to temp folder,
		os.chdir(wd+'temp')	
		# concatenation
		subprocess.call('cp ../concatenation_files/* .', shell = True)
		subprocess.call('cat client_lat_1 client_lat_2 client_lat_3 > client_latency.txt', shell = True)
		subprocess.call('cat client_time_1 client_time_2 client_time_3 > client_time.txt', shell = True)
		subprocess.call('cat util_1 util_2 util_3 > link_util.txt', shell = True)
		subprocess.call('cat time_1 time_2 time_3 > link_time.txt', shell = True)
		subprocess.call('cat lat.log_1 lat.log_2 lat.log_3 > lat.log', shell = True)
		
		# append matlab script format, make addition
		subprocess.call('cat ctime1 client_time_1 close ctime2 client_time_2 close ctime3 client_time_3 close clat1 client_lat_1 close clat2 client_lat_2 close clat3 client_lat_3 close util1 util_1 close util2 util_2 close util3 util_3 close time1 time_1 close time2 time_2 close time3 time_3 close matlab_tail title> matlab_plot.txt', shell = True)			
		# move to official folder
		os.chdir(wd)
		subprocess.call('mkdir '+wd+'5LM/'+link+'/', shell = True)
		os.chdir(wd+'temp')
		subprocess.call('mv matlab_plot.txt client_latency.txt client_time.txt link_util.txt link_time.txt lat.log msg.txt '+wd+'5LM/'+link+'/', shell = True)

		# make sure temp is empty
		subprocess.call('rm '+wd+'temp/*', shell = True)


