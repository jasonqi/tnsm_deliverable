#!/usr/bin/python

import re
import os
import sys
from optparse import OptionParser

if __name__ == '__main__':
	time_list = []
    	n_req_list = []
   	parser = OptionParser()
    	parser.add_option("-i", "--inputtrace", dest="tracefile", help="Input trace file")
    	(options, args) = parser.parse_args()
    	if options.tracefile== None:
     		sys.exit("Error: Not all options are set! Use -h, for help")
    	try:
        	f_trace = open(options.tracefile, 'r')
   	except:
       	 	sys.exit("Error while opening trace file")
    	for line in f_trace:
		new_tstamp = int(line.split(',')[0])
		if len(time_list) == 0:
			time_list.append(new_tstamp)
                        n_req_list.append(1)
			continue
		if new_tstamp != time_list[len(time_list)-1]:
			time_list.append(new_tstamp)
			n_req_list.append(1)
		else:
			n_req_list[len(n_req_list)-1] +=1
	f_trace.close()
	try:
                f_out = open("rdm"+ options.tracefile, 'w')
        except:
                sys.exit("Error while opening output file")
	for i in range(0, len(time_list)):
		delta_t = round (1.0 / n_req_list[i], 3)
		for j in range(0, n_req_list[i]):
			f_out.write(str(time_list[i] + delta_t*j) + '\n')
	f_out.close()

