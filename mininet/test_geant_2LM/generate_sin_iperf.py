#!/usr/bin/python

import re
import os
import sys
import math
from time import sleep
from optparse import OptionParser

if __name__ == '__main__':
	parser = OptionParser()
	parser.add_option("-i", "--interval", dest="interval", help="Time interval of a constant bandwidth iperf in seconds")
	parser.add_option("-p", "--period", dest="period", help="Time period of a sinusoidal iperf traffic in seconds")
	parser.add_option("-d", "--destination", dest="destination", help="Address of destination")
	(options, args) = parser.parse_args()

	if options.interval == None or options.period == None or options.destination == None:
		sys.exit("Error: Not all options are set! Use -h, for help")
	
	i=0
	period = float(options.period)
	interval = float(options.interval)
	os.system("echo "+str(interval))
	while i < period/interval: 
		os.system("iperf -t "+str(interval)+" -c "+options.destination+" -u -b "+str(5*(math.sin(6.283*i*interval/period))+5)+"M &")
		sleep(interval)
		i += 1

