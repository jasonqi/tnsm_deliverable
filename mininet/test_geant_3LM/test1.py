#!/usr/bin/python

"""
This script:
1) LOADS TOPOLOGY/CLIENTS/SERVERS/CLUSTERS

2) COMPUTE PATHS (YEN'S ALGORITHM)
"""
from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, Intf
from mininet.link import TCLink
from optparse import OptionParser
import re
import os
import sys

#import my k-shortest path module (based on Yen's algorithm)
import module_yens_algo

INIT_CTRL_PORT = 6633 #controller 0 listening on 6633, controller 1 on 6634 etc...
LINK_LATENCY = 10 #by default 10 ms for every link

def ExampleTopo():

    parser = OptionParser()
    parser.add_option("-i", "--inputdir", dest="directory", help="Directory containing the input files (switch, mgr_clusters, clients, servers)", default=None)
    parser.add_option("-o", "--outputdir", dest="out_directory", help="Directory containing the output files (topo, paths, servers)", default=None)

    (options, args) = parser.parse_args()
    if options.directory == None or options.out_directory == None:
    	sys.exit("Error: Not all options are set! Use -h, for help")

    #create the remote controller objects (managers will operate on top of them)
    controllers = {}
    try:
        f_clusters = open(options.directory + "/mgr_clusters.txt", 'r')
    except:
        sys.exit("Error: While opening mgr_clusters.txt")
        return 1
    for line in f_clusters:
	line = line[:len(line)-1]
	line = line.split('|')
	controllers[line[0]] = RemoteController('c' + line[0], ip='127.0.0.1', port=INIT_CTRL_PORT+int(line[0]) )
    print controllers
   
    #store managers' ip and port
    try:
        f_mgr_ip_ports = open(options.out_directory + "/mgr_ip_port.txt", 'w')
    except:
        sys.exit("Error: While opening mgr_ip_ports.txt")
        return 1
    for ctrl in controllers:
	f_mgr_ip_ports.write(ctrl + " " + str(controllers[ctrl].ip) + " " + str(controllers[ctrl].port) + '\n')
    f_mgr_ip_ports.close()
	
 
    #create an empty mininet network
    net = Mininet( topo=None,
                   build=False, autoSetMacs=True, link=TCLink)


    #create a custom link dictionary (htb : Hierarchical Token Bucket) TODO Customize it, if needed
    link_stat_test = dict(bw=10, delay='4ms', loss=0, max_queue_size=100, use_htb=True)

    #create a custom link dict for server links
    link_stat_test_server = dict(bw=100, delay='4ms', loss=0, max_queue_size=100, use_htb=True)

    #open topology file
    try:
	f_topo = open(options.directory + "/topo.txt", 'r')
    except:
        sys.exit("Error: While opening topo.txt")
    #parse the topology file and add the edges to the mininet topology
    d = {} #switch dict
    for line in f_topo:
    	line = line[:len(line)-1]
        line = line.split(" ")
	#load switches into the datastore (if not done yet)
	if ('s' + line[0]) not in d:
		d['s' + line[0]] = net.addSwitch('s' + line[0])
        if ('s' + line[1]) not in d:
                d['s' + line[1]] = net.addSwitch('s' + line[1])
        #add a network edge for each line of the input file
        net.addLink(d['s' + line[0]], d['s' + line[1]], **link_stat_test)
    print "SWITCHES:"
    print d

    #load clients and servers and add client-switch and server-switch links
    host_ip = {} #keep track of every host ip address
    clients = {}
    try:
        f_clients = open(options.directory + "/clients.txt", 'r')
    except:
        sys.exit("Error: While opening clients.txt")
        return 1
    for line in f_clients:
        line = line[:len(line)-1]
        line = line.split(" ")
        clientname = line[0]
        clients[clientname] = net.addHost( clientname, ip=line[1])
        host_ip[clientname] = line[1]
        net.addLink( clients[clientname], d[line[2]], **link_stat_test)
    print "CLIENTS:"
    print clients
    
    servers = {}
    try:
        f_servers = open(options.directory + "/servers.txt", 'r')
    except:
        sys.exit("Error: While opening servers.txt")
        return 1
    for line in f_servers:
        line = line[:len(line)-1]
        line = line.split(" ")
        servername = line[0]
        servers[servername] = net.addHost( servername, ip=line[1])
        host_ip[servername] = line[1]
        net.addLink( servers[servername], d[line[2]], **link_stat_test_server)
    print "SERVERS:"
    print servers

    #add the controllers to the network
    for ctrl in controllers:
	net.addController(controllers[ctrl])

    net.build()

    #associate the switches to the corresponding controllers
    f_clusters.seek(0, 0)
    clusters = {}
    for line in f_clusters:
        line = line[:len(line)-1]
        spl = line.split("|")
        ctrl = spl[0]
        clusters[ctrl] = []
        sw_list = spl[1].split(";")
        del(sw_list[len(sw_list)-1])
        for item in sw_list:
		clusters[ctrl].append('s'+item)
       		#only mgr1 and mgr2..
		d['s'+item].start( [controllers[ctrl]] )
    print clusters

    #set static arp entries for every possible pair (client, server)
    for clientname in clients:
	for servername in servers:
		clients[clientname].cmd('arp -s ' + servers[servername].IP() + ' ' + servers[servername].MAC()) #static arp entry client--> server
        	servers[servername].cmd('arp -s ' + clients[clientname].IP() + ' ' + clients[clientname].MAC()) #static arp entry server--> client


    #fill output link-port file
    print "Creating topology file..."
    try:
        f_out = open(options.out_directory + "/out_links_if.txt", 'w')
    except:
        sys.exit("Error: While opening output file")
        return 1
    x=1
    for l in  net.links:
        f_out.write('L' + str(x) + ' ' + str(l)+"\n")
	#f_out.write(str(l) + '\n')
	x+=1
    f_out.close()
    os.system("sed -i 's/<->/ /g' "+ options.out_directory + "/out_links_if.txt")
    os.system("sed -i 's/-/ /g' "+ options.out_directory + "/out_links_if.txt")

    #compute paths
    try:
    	os.system("rm " + options.out_directory + "/sp_output*")
    except:
	print "No prior sp_output.txt file found. Ok"

    print "Computing paths (based on yen's k-shortest path algorithm)..."
    try:
        f_paths = open(options.directory + "/src_dst_npaths.txt", 'r')
    except:
        sys.exit("Error: While opening source,dest,n. paths file")
        return 1
    for line in f_paths:
	line = line.split(' ')
	src = line[0]
	dst = line[1]
	n_paths = int(line[2])
	try:
		path_list = module_yens_algo.yens_algo(options.directory + "/topo.txt", n_paths, src, dst)
		module_yens_algo.save_on_file(path_list, src, dst, options.out_directory)
	except:
		sys.exit("Unable to run Yen's k-shortest paths algorithm. Check the arguments.")
    
    #build mgr-sw and mgr-mgr distancy files
    for ctrl in controllers:
        #open file
        f_lat = open(options.out_directory + "/mgr" + ctrl + "_latencies.txt", 'w')
        for sw in d:
                sw_id = sw[1:] #strip 's'
                if sw_id == ctrl:
                        f_lat.write("mgr" + ctrl + " " + sw + " 5\n") #5 ms by default if the manager is on the switch 
                        continue
                path = module_yens_algo.yens_algo(options.directory + "/topo.txt", 1, ctrl, sw_id)[0]
                #print path
                f_lat.write("mgr" + ctrl + " " + sw + ' ' + str((len(path)-1)*LINK_LATENCY) + '\n')
        f_lat.close()

    for sw in d:
	f_lat = open(options.out_directory + "/latencies/mgr" + sw[1:] + "_latencies.txt", 'w')
	sw_id = sw[1:]
	for oth_sw in d:
		oth_sw_id = oth_sw[1:]
		if oth_sw_id == sw_id:
			f_lat.write("mgr" + ctrl + " " + oth_sw + " 5\n") #5 ms by default if the manager is on the switch
			continue
		path = module_yens_algo.yens_algo(options.directory + "/topo.txt", 1, sw_id, oth_sw_id)[0]
 		f_lat.write("mgr" + ctrl + " " + oth_sw + ' ' + str((len(path)-1)*LINK_LATENCY) + '\n')

	f_lat.close()

    #copy mgr_clusters.txt into output dir
    try:
        os.system("cp " + options.directory + "/mgr_clusters.txt " + options.out_directory + "/")
    except:
        print "Error while copying file mgr_clusters.txt to output dir"

    #copy servers.txt into output dir
    try:
        os.system("cp " + options.directory + "/servers.txt " + options.out_directory + "/")
    except:
        print "Error while copying file servers.txt to output dir "

    #check the path file
    n_paths = 0
    f_path = open(options.out_directory + "/sp_output.txt", 'r')
    f_path.seek(0,0)
    for line in f_path:
	n_paths +=1
    
    #build an initial map clients-paths mapping
    #print "Binding initial client-path mapping (round-robin)"
    #try:
        #f_bind = open(options.out_directory + "/path_client.txt", 'w')
    #except:
        #sys.exit("Error: While creating path_client.txt...")
    #
    #init = 1
    #f_clients.seek(0,0)
    #for line in f_clients:
	#print "Binding client " + line + ". Start with path " + str(init)
	#line = line.split(" ")
	#clientname = line[0]
	#clientip = line[1]
	#client_sw_n = line[2][1:]
	#path_id  = find_path(client_sw_n, n_paths, options.out_directory + "/sp_output.txt", init)
	#print "Path: " + path_id
	#if path_id == '0':
	#	print "Error: Can't bind the client to any path!!"
	#else:
	#	f_bind.write(path_id + " " + clientname + " " + clientip + '\n')	
	#	init = int(path_id)	
	#	print path_id + " " + clientname + " " + clientip    

    #start CLI
    CLI( net )
    net.stop()
		

def find_path(sw_n, n_paths, f_path_name, init):
    try:
        f_path = open(f_path_name, 'r')
    except:
        sys.exit("Error: While creating path_client.txt...")
    f_path.seek(0,0)

    print "Looking for a path from switch s" + sw_n + ". Start from path " + str(init)  
    curr_line = init
    for i in range(0, init-1):
	line = f_path.readline()
	print line
    for i in range(init, n_paths+1):
	line = f_path.readline()
	print line
    	line = line.split(" ")
        if line[0] == sw_n:
		print str(curr_line)
        	return str(curr_line)
	else:
		curr_line+=1
    curr_line = 1
    f_path.seek(0,0)
    for i in range(0, init-1):
	line = f_path.readline()
	line = line.split(" ")
        if line[0] == sw_n:
                return str(curr_line)
        else:
                curr_line+=1
    f_path.close()
    return '0'

if __name__ == '__main__':
    setLogLevel( 'info' )
    ExampleTopo()
