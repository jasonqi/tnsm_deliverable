#! /usr/bin/python

import re
import os
import sys
import random
from time import sleep
from optparse import OptionParser

if __name__ == '__main__':
        parser = OptionParser()
        parser.add_option("-d", "--destination", dest="destination", help="Address of destination")
        (options, args) = parser.parse_args()

        if options.destination == None:
                sys.exit("Error: Not all options are set! Use -h, for help")

       	# initiate the background traffic (5M) for 3hr
	os.system("iperf -t 14400 -c "+options.destination+" -u -b 5M &")
	while True:
		# generate iperf traffic with uniformly distributed bandwidth and duration
		os.system("iperf -t "+str(0.001*random.randrange(10,15001))+" -c "+options.destination+" -u -b "+str(0.001*random.randrange(5000))+"M &")
		# Poisson arrival: mean = 10 seconds
		sleep(random.expovariate(0.1))
  
