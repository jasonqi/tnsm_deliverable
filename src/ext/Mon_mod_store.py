from pox.core import core
import sys
log = core.getLogger()

DEFAULT_MGR = 0

class MonitoringModuleStore (object):
        def __init__(self, manager):
                #Declare main data strucures (cluster swich list, topology, paths)
                self.sw_cluster = []
                self.topo_store = {}
                self.path_store = {}
                #Additional data structure (switch - list of ports)
                self.sw_port_store = {}
                #Bind the MonitoringModule to the corresponding Manager
                self.mgr = manager
                log.debug("Initializing Monitoring Module for Manager %d", self.mgr)
                #Initialize local data structures
                self.init_sw_cluster()
                self.init_topo_store()
                self.init_path_store()

		"""
		Additional : latency table
		"""
		self.mgr_lat = {}
		self.init_mgr_lat()

        def init_sw_cluster(self):
                try:
            	       clus_f = open("./config/mgr_clusters.txt", 'r')
            	except:
            	       sys.exit("Error: While opening the switch-cluster file")
                for line in clus_f:
                        line = line[:len(line)-1]
                        spl = line.split("|")
                        mgr = spl[0]
                        if int(mgr) == self.mgr:
                                sw_list = spl[1].split(";")
                                del(sw_list[len(sw_list)-1])
                                for item in sw_list:
                                        self.sw_cluster.append('s'+item)
		log.debug("Local cluster: ")
                print self.sw_cluster


        #Simple topology data store loading, based on the toy experiment scenario
        def init_topo_store(self):
                try:
                	topo_f = open("./config/out_links_if.txt", 'r')
        	except:
        		sys.exit("Error: While opening the links-interface file")
                for line in topo_f:
                        line = line[:len(line)-1]
                        line = line.split(" ")
                        self.topo_store[line[0]] = [line[1], line[2], line[3], line[4]]
                        #fill the additional switch-ports store
                        if line[1] not in self.sw_port_store:
                                self.sw_port_store[line[1]] = []
                        if line[3] not in self.sw_port_store:
                                self.sw_port_store[line[3]] = []
                        if line[2] not in self.sw_port_store[line[1]]:
                                self.sw_port_store[line[1]].append(line[2])
                        if line[4] not in self.sw_port_store[line[3]]:
                                self.sw_port_store[line[3]].append(line[4])
                log.debug("Topology Store:")
                print self.topo_store
                log.debug("Additional Switch-Port Store:")
                print self.sw_port_store


        def init_path_store(self):
                try:
                	path_f = open("./config/mgr" + str(self.mgr) + "_installed_path.txt", 'r')
        	except:
        	        sys.exit("Error: While opening the path file")
                for line in path_f:
                        line = line[:len(line)-1]
                        line = line.split(" ")
                        path_key = 'P' + line[0]
                        sw_list = line[1].split(",")
                        del(sw_list[len(sw_list)-1])
                        link_list = []
                        prev = 0
                        for switch in sw_list[1:]:
                                end_sw_1 = switch
                                end_sw_2 = sw_list[prev]
                                #search corresponding link in the topo_store
                                for key, value in self.topo_store.iteritems():
                                        if 's'+end_sw_1 == value[0] and 's'+end_sw_2 == value[2] or 's'+end_sw_1 == value[2] and 's'+end_sw_2 == value[0]:
                                                link_list.append(key)
                                prev +=1
                        if link_list == []:
                                log.info("Error: Link not found. Skipping...")
                                continue
                        self.path_store[path_key] = ['s'+sw_list[0], line[2], link_list, int(line[3]), 's'+sw_list[-1]]

                log.debug("Path store:")
                print self.path_store

	def init_mgr_lat(self):
		try:
                        lat_f = open("./config/mgr" + str(self.mgr) + "_latencies.txt", 'r')
                except:
                        sys.exit("Error: While opening the mgr latency file")
		for line in lat_f:
			line = line[:len(line)-1]
			line = line.split(" ")
			self.mgr_lat[line[1]] = float(line[2])
		log.debug("Latency store")
		print "((((((((((((((((((((((((((((((((((((((("
		print self.mgr_lat
		print ")))))))))))))))))))))))))))))))))))))))"
def launch (manager = DEFAULT_MGR):
	core.registerNew(MonitoringModuleStore, int(manager))
