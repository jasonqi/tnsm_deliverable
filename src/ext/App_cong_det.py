import const
import time
from pox.core import core
from pox.lib.recoco import Timer
from datetime import datetime
from time import sleep

DEFAULT_MGR = 0
DEFAULT_DISS_POLICY = 0 #Default policy for info dissemination to remote app instance : send info every time there is new data available

log = core.getLogger()

class AppCongestionDetection(object):
        def __init__(self, manager, policy):
                self.mgr = manager
                self._diss_policy = policy

                self._cong_threshold = const.LINK_CAPACITY * const.CONG_THR
                self._link_store = {} # Local links --> Link Id. : / Util(Mbps) / AvailableBw(Mbps) / Congested (TRUE/FALSE) / Time
                self._rem_app_interests = {} #Interest of remote cong.det. instances in local links --> Link, list_of_app_id

                self.init_link_stores()
                #Timer(10.0, self.load_mon_requirement, args=[], recurring=False) Automatic


        def init_link_stores(self):
                #initialize link stores
                for link, ifs in core.MonitoringModuleStore.topo_store.iteritems():
                        if ifs[0] in core.MonitoringModuleStore.sw_cluster or ifs[2] in core.MonitoringModuleStore.sw_cluster:
                                self._link_store[link] = None
                                self._rem_app_interests[link] = []
                print "LOCAL LINK STORE"
                print self._link_store


        def load_mon_requirement(self):
                #load the requirement, based on the Manager id
                try:
                        f_req = open("./config/cd.txt", 'r')
                        for line in f_req:
                                line = line.split(" ")
                                if line[0] == "CD_" + str(self.mgr): #select line(s) of the corresponding CD app. instance
                                        link_list = []
                                        for link in line[1].split(','):
                                                if link in self._link_store:
                                                        link_list.append(link)
                                        if link_list == []:
                                                continue
                                        req_mon_freq = float(line[2])
                                        #register the new local monitoring requiement to the Monitoring Module
                                        new_mon_requirement = ["CD_"+str(self.mgr), ["LINK", "UTIL", ""], link_list, req_mon_freq]
                                        core.MonitoringModuleRequirementInterpreter.register_req(new_mon_requirement)
                                else:
                                        #register remote apps. interest on local links.
                                        link_list = line[1].split(',')
                                        for link in link_list:
                                                if link in self._link_store:
                                                        self._rem_app_interests[link].append(line[0])

                except Exception as exc:
            	        sys.exit("Error: While loading the congestion detection requirement file")
                print "REMOTE INSTANCES INTERESTED IN LOCAL LINKS"
                print self._rem_app_interests



        #Receive a String msg, format it, make it available to the other apps.
        def handle_msg_from_udp_if(self, msg, mgr_id):
                log.info("Link update received from app. %s: %s", mgr_id, msg)
                #Parse the string msg
                splitted_msg = msg.split(',')
                app = splitted_msg[0]
                link_id = splitted_msg[1]
                tx_mbps = float(splitted_msg[2])
                rx_mbps = float(splitted_msg[3])
                available_tx_mbps = float(splitted_msg[4])
                available_rx_mbps = float(splitted_msg[5])
                congested_flag = int(splitted_msg[6])
                time = splitted_msg[7]
		#log
		rx_latency = core.MonitoringModuleStore.mgr_lat['s' + app[3:]] #distance traveled by info (ms)
		f_log = open('lat.log', 'a')
                f_log.write(link_id + " RE_RECONF " + str(max(tx_mbps, rx_mbps)) + ' ' + str(datetime.now().time()) + ' ' + str(const.LINK_UPDATE_BYTES) + ' ' + str(rx_latency) + ' LM_' + str(self.mgr)+'\n')
                f_log.close()
		if hasattr(core, 'AppPathSelection'):
                	#share the info with the local App Path Selection (if any)
                	core.AppPathSelection.handle_update_from_app_cong_det(link_id, available_tx_mbps, available_rx_mbps, time, congested_flag)
		if hasattr(core, 'AppRedirect'):
                        #share the info with the local App Path Selection (if any)
                        core.AppRedirect.handle_update_from_app_cong_det(link_id, available_tx_mbps, available_rx_mbps, time, congested_flag)



        #Handle an update of link util from the local MonitoringModule
        #Update is a list of tuples (NOTICE: by default, 1 tuple) [Link Id, Avg Util (over last time-window), Time of Measurement(datetime.time)]
        def handle_link_util_updates(self, update_msg):
                log.debug("Update received from MonitoringModule at time: %s", str(datetime.now().time()))
		f_log2 = open('util_pox.log', 'a')
		f_log3 = open('time_pox.log','a')
		#print "-------------------------------------------------------------------"
		#print "&&&&&&&&&&&&"
		#print "core.MonitoringModuleRequirementInterpreter.low_level_targets = ",core.MonitoringModuleRequirementInterpreter.low_level_targets
		#print "&&&&&&&&&&&&"
                for link_info in update_msg:
                        link_id = link_info[0]
                        link_avg_util_tx = (link_info[1] * 8) / 1000000.0 #from bytes per sec. to Mbps
                        link_avg_util_rx = (link_info[2] * 8) / 1000000.0 #from bytes per sec to Mbps
                        meas_arrival_t = link_info[3]

                        log.info("Link: %s, Avg_Util(tx): %f, Avg_Util(rx): %f, Time: %s", link_id, link_avg_util_tx, link_avg_util_rx, str(meas_arrival_t.time()))
                        if link_id == 'L14':
				f_log2.write(str(max(link_avg_util_tx,link_avg_util_rx))+'\n')
				f_log3.write(str(time.time())+'\n')
			#f_log2.write(str(max(link_avg_util_tx,link_avg_util_rx))+'\n')
			#f_log3.write(str(time.time())+'\n')
                        #compute available bw (Mbps) on link
                        link_available_bw_tx = const.LINK_CAPACITY - link_avg_util_tx
                        link_available_bw_rx = const.LINK_CAPACITY - link_avg_util_rx

                        #check if the link is congested (on threshold basis)
                        congested_flag = 0
                        if max(link_avg_util_tx,link_avg_util_rx) > self._cong_threshold :
                                log.info("Link %s congested", link_id)
				#find how many paths go through this link
                                congested_flag = 1
                                for path_id in core.MonitoringModuleStore.path_store:
                                        if link_id in core.MonitoringModuleStore.path_store[path_id][2]:
                                                congested_flag+=1

                        #update local link store
                        self._link_store[link_id] = [link_available_bw_tx, link_available_bw_rx, congested_flag]

			#log
			f_log = open('lat.log', 'a')
			f_log.write(link_id + " LO_RECONF " + str(max(link_avg_util_tx,link_avg_util_rx)) + ' ' + str(datetime.now().time()) + ' ' + str(const.LINK_UPDATE_BYTES) + ' LM_' + str(self.mgr) + '\n')
			f_log.close()

                        #Deliver the data to the local Path Selection Application (if any)
                        if hasattr(core, 'AppPathSelection'):
                                core.AppPathSelection.handle_update_from_app_cong_det(link_id, link_available_bw_tx, link_available_bw_rx, str(meas_arrival_t.time()), congested_flag)

			#Deliver the data to the local Redirection Application (if any)
                        if hasattr(core, 'AppRedirect'):
                                core.AppRedirect.handle_update_from_app_cong_det(link_id, link_available_bw_tx, link_available_bw_rx, str(meas_arrival_t.time()), congested_flag)

			#Deliver the data through udp interface if other cong.det. instances are interested in this link
                        rem_app_list = []
                        for app_id in self._rem_app_interests[link_id]:
                                rem_app_list.append(app_id)
                        if rem_app_list != []:
                                #the update is packed in a string msg
                                string_msg = "CD_" + str(self.mgr) + "," + link_id + ',' + str(link_avg_util_tx) + ',' + str(link_avg_util_rx) + ',' + str(link_available_bw_tx) + ',' + str(link_available_bw_rx) + ',' + str(congested_flag) + ',' + str(meas_arrival_t.time()) + ';'
                                #the string msg is sent through udp if
				core.UDPInterface.udp_tx(string_msg, rem_app_list)
		f_log2.close()
		f_log3.close() 
def launch(manager = DEFAULT_MGR, policy = DEFAULT_DISS_POLICY):
        core.registerNew(AppCongestionDetection, int(manager), int(policy))
