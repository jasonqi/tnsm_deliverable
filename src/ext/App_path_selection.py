from pox.core import core
from pox.lib.recoco import Timer
from datetime import datetime
#library imported for offloading decision enforcement
import pox.openflow.libopenflow_01 as of
from pox.lib.packet.ethernet import ethernet
from pox.lib.addresses import IPAddr, EthAddr
from time import sleep
import const
import math
import sys
DEFAULT_MGR = 0

"""
offloading policies
"""
NO_OFFLOAD = 0 #disabled
EQ_OFFLOAD = 1 #offload equally to non-congested paths
DIST_OFFLOAD = 2 #offload to non-congested paths inversely proportional to path's number of hops
WE_OFFLOAD = 3 #offload weighted on the residual bandwidth of non congested paths

"""
path state (available, congested, offloading)
"""
AVAILABLE = 0 #the path can enter an "offloading" procedure
OFFLOADING = 2 #the path is on backoff (to limit adaptation rate)


log = core.getLogger()

class AppPathSelection(object):
        def __init__(self, manager, offloading):
                self.mgr = manager
                self.offload_policy = offloading
                self._store = {} # it stores the (minimum) available bandwidth on the paths + flow rates (1 flow = 1 client)
                self._cong_threshold = const.LINK_CAPACITY * const.CONG_THR
                self._offload_backoff = 3 # 3 secs backoff for congested path
                #self.load_mon_requirement() #automatic



        def load_mon_requirement(self):
                #load the requirement, based on the Manager id
                try:
                        f_req = open("./config/ps.txt", 'r')
                        for line in f_req:
                                line = line.split(" ")
                                if line[0] == "PS_" + str(self.mgr): #select line(s) of the corresponding CD app. instance
                                        path_list = []
                                        for path in line[1].split(','):
                                                if core.MonitoringModuleStore.path_store[path][0] in core.MonitoringModuleStore.sw_cluster:
                                                        path_list.append(path)
                                        if path_list == []:
                                                continue
                                        req_mon_freq = float(line[2])
                                        #register the new local monitoring requiement to the Monitoring Module
                                        new_mon_requirement = ["PS_"+str(self.mgr), ["PATH", "UTIL", "flow"], path_list, req_mon_freq]
                                        core.MonitoringModuleRequirementInterpreter.register_req(new_mon_requirement)
                except Exception as exc:
			print "Exception!! ",exc
            	       	sys.exit("Error: While loading the path selection requirement file")


        #handle link update (one update per link)
        def handle_update_from_app_cong_det(self, link_id, available_tx_mbps, available_rx_mbps, time, congestion):
                log.debug("Update received by the Congestion Detection App. for link %s", link_id)
                #find what are the paths associated to the link
		#used_flow = []
                offloaded = 0.0 #offloaded mbps
		for path_id in core.MonitoringModuleStore.path_store:
                        if link_id in core.MonitoringModuleStore.path_store[path_id][2]:
                                #Update local store (Available bandwidth should be determined for the two dir.)
                                if path_id not in self._store:
                                        #add new entry
                                        self._store[path_id] = [AVAILABLE, {}, {}]
                                        self._store[path_id][1][link_id] = min(available_tx_mbps, available_rx_mbps)
                                else:
                                        self._store[path_id][1][link_id] = min(available_tx_mbps, available_rx_mbps)
                                        #decide on offloading (congestion = num. paths on congested link + 1, 0 if no congestion)
                                        if congestion > 0 and self._store[path_id][0] == AVAILABLE and link_id == const.CONG_LINK:
                                                #freeze the path (e.g. 10 secs backoff) and start offloading procedure
                                                self._store[path_id][0] = OFFLOADING
                                                Timer(self._offload_backoff, self.unlock_path, args=[path_id], recurring=False)
                                                offloaded = offloaded + self.offload(path_id, min(available_tx_mbps, available_rx_mbps), congestion,offloaded)
                                        	print "Offloaded = " + str(offloaded)
						if offloaded > 0:
							f_log = open('lat.log', 'a')
							f_log.write(link_id + " OFFLOADING PS_" + str(self.mgr) + ' '  + str(datetime.now().time()) + ' LM_' + str(self.mgr) + '\n')

                					f_log.close() 
					elif congestion > 0 and self._store[path_id][0] != AVAILABLE:
                                                log.debug("PATH %s : BACKOFF", path_id)


        def unlock_path(self, path_id):
                #unlock path
                self._store[path_id][0] = AVAILABLE

        """
        DEPRECATED
        # This is only a simple example. Available bandwidth should be determined for the two senses:
        #   client --> server
        #   server --> client
        def update_store(self, path_id, link_id, available_mbps):
                if path_id not in self._store:
                        #add new entry
                        self._store[path_id] = [None, {}, {}]
                        self._store[path_id][1][link_id] = available_mbps
                else:
                        #update existing entry
                        self._store[path_id][1][link_id]== available_mbps
                self._store[path_id][0] = min( (self._store[path_id][1]).values() )
        """


        #Receive Path Traffic (per flow/client_ip) updates from the Monitoring Module
        def handle_path_util_updates(self, update_msg):
                for flow_info in update_msg:
                        path_id = flow_info[0]
                        client_ip = flow_info[1]
                        curr_rate_mbps = (flow_info[2] * 8) / 1000000.0
                        meas_time = flow_info[3]
                        if path_id not in self._store:
                                #add a new entry
                                self._store[path_id] = [AVAILABLE, {}, {}]
                                self._store[path_id][2][client_ip] = [curr_rate_mbps, meas_time]
                        else:
                                #update existing entry
                                self._store[path_id][2][client_ip] = [curr_rate_mbps, meas_time]
                print '\n'
		log.info("Updated Local Path Store at time %s", str(datetime.now().time()))
                print self._store



        """
        Path Offloading methodsi
        """
        #FIXME the available bw. of the alternative paths can be saturated due to lack of coordination between
        #      decision-making points.


        #Offloading Interface
        def offload(self, path_id, available_bw, congestion, offloaded):
                if self.offload_policy == NO_OFFLOAD:
                      return
                elif self.offload_policy == EQ_OFFLOAD:
                        return self.equally_offload(path_id, available_bw, congestion, offloaded)
                elif self.offload_policy == DIST_OFFLOAD:
                        self.dist_offload(path_id, available_bw, congestion)
                elif self.offload_policy == WE_OFFLOAD:
                        self.weight_offload(path_id, available_bw, congestion)



        def equally_offload(self, path_id, available_bw, congestion, offloaded):
                log.info("PATH %s : OFFLOADING  (policy = 1)", path_id)
                #traffic decrement (congestion = num. paths on congested link + 1, 0 if no congestion)
                exceeding_bw = (const.LINK_CAPACITY - available_bw) - self._cong_threshold - offloaded
                log.info("Path %s: -%f", path_id, exceeding_bw)
		#select flows to be moved to alternative path (decreasing first fit). 1 flow = 1 client ip
                #decr_rate_flows = sorted( self._store[path_id][2], key=(self._store[path_id][2]).get)

                #find alternative path candidates and compute traffic shifts
                alternative_paths = []
                alternative_paths_min_av_bw = {}
                server = core.MonitoringModuleStore.path_store[path_id][1]
		ingress = core.MonitoringModuleStore.path_store[path_id][0]
		total_decr = 0.0 #total decrement
                for key, entry in core.MonitoringModuleStore.path_store.iteritems():
                        if key != path_id and entry[0] == ingress: #TODO restore: and entry[1] == server
				'''
                                #check if the candidate path can accept traffic
                                if self._store[key][0] == AVAILABLE:
                                        alternative_paths.append(key)
                                        alternative_paths_min_av_bw[key] = min( (self._store[key][1]).values() )
				'''
				try:
					#check if the candidate path can accept traffic
                                	if self._store[key][0] == AVAILABLE:
                                        	alternative_paths.append(key)
                                        	alternative_paths_min_av_bw[key] = min( (self._store[key][1]).values() )
				except Exception as exc:
					print "EXCEPTION:", exc
                if alternative_paths == []:
                        log.debug("Alternative path set = []. Abort...")
                        return total_decr 
                #select flows to be moved to alternative path (decreasing first fit). 1 flow = 1 client ip
                decr_rate_flows = sorted( self._store[path_id][2], key=(self._store[path_id][2]).get)
		decr_rate_flows.remove('10.0.0.5')
		#TODO:Sort alternative paths according to util margin
		#Cut extra alternative paths
		#alternative_paths = alternative_paths[0:len(decr_rate_flows)-1]		

                #if alternative_paths == []:
                #        log.debug("Alternative path set = []. Abort...")
                #        return total_decr
		
		#the traffic decrement is equally splitted among the alternative paths
                per_path_increment = exceeding_bw / len(alternative_paths)
		#evenly distribute clients to each alternative path
		used_flow = []
		if len(decr_rate_flows) == 0:
			target_client_number = 0
		else:		
			target_client_number = math.ceil(len(decr_rate_flows)/float(len(alternative_paths)))
                offloading_output = {} #result of the offloading algorithm
                for path in alternative_paths:
			used_num = 0
                        log.info("Path %s: +%f", path, per_path_increment)
                        increment = 0.0
                        offloading_output[path] = []
			#used_flow = []
			print "decr_rate_flows = ", decr_rate_flows
			print "@@@@@@@@@@@@@@@@Target client numbebr = ", target_client_number
			print "@@@@@@@@@@@@@@@@Path ID = ", path
			print "@@@@@@@@@@@@@@@@Used_Flow = ",used_flow
			print "@@@@@@@@@@@@@@@@Used_num = ",used_num
                        for flow in decr_rate_flows:
				print "self._store[path_id][2][flow][0] = ", self._store[path_id][2][flow][0]
                                #if used_num < target_client_number and flow not in used_flow and self._store[path_id][2][flow][0] > 0.0:  #zero-traffic flows are not considered
				if used_num < target_client_number and flow not in used_flow:
					print "+++++++FIRST IF PASSED++++++"
				#if self._store[path_id][2][flow][0] > 0.0:  #zero-traffic flows are not considered
                                        #if (increment + self._store[path_id][2][flow][0]) > per_path_increment or (increment + self._store[path_id][2][flow][0]) > alternative_paths_min_av_bw[path]:
					if (increment + self._store[path_id][2][flow][0]) > alternative_paths_min_av_bw[path]:
						print "++++++++SECOND IF FAILED++++++++"
                                                break
						#continue
                                        else:
						used_flow.append(flow)
                                                increment += self._store[path_id][2][flow][0]
						total_decr+= self._store[path_id][2][flow][0]
                                                offloading_output[path].append(flow)
						#print used_flow
						used_num = used_num + 1
			print "-----------------------------------------------------------"
                log.info("OFFLOADING OUTPUT (alternative_path : [flow/client_ip]): ")
                print offloading_output
                #call reconfiguration enforcement procedure
		self.enforce_offload(path_id, offloading_output)
		"""
		test
		"""
		"""		
		for path, flows in offloading_output.iteritems():
			for flow in flows:
				core.AppRedirect.adapt_mapping_from_path_sel(flow)
		"""
		files = [1,2,3,4]
                for i in files:
                	f = open("/home/mininet/test_abilene/"+str(i)+"_http_output.txt",'a')
			f.write("PATH "+path_id+ ": OFFLOADING  (policy = 1)\n")
			f.write("OFFLOADING OUTPUT (alternative_path : [flow/client_ip]): "+str(offloading_output)+"\n\n")
                        f.close()
		return total_decr

        def dist_offload(self, path_id, available_bw, congestiom):
                log.info("PATH %s : OFFLOADING  (policy = 2)", path_id)
                #traffic decrement (congestion = num. paths on congested link + 1, 0 if no congestion)
                exceeding_bw = ((const.LINK_CAPACITY - available_bw) - self._cong_threshold) / (congestion - 1)
                log.info("Path %s: -%f", path_id, exceeding_bw)
                #find alternative path candidates (+ number of hops) and compute traffic shifts
                alternative_paths = []
                alternative_paths_lengths = {}
                alternative_paths_min_av_bw = {}
                sum_lengths = 0.0
                server = core.MonitoringModuleStore.path_store[path_id][1]
		ingress = core.MonitoringModuleStore.path_store[path_id][0]
                for key, entry in core.MonitoringModuleStore.path_store.iteritems():
                        if key != path_id and entry[1] == server and entry[0] == ingress:                
				#check if the candidate path can accept traffic
                                if self._store[key][0] == AVAILABLE:
                                        alternative_paths.append(key)
                                        alternative_paths_lengths[key] = len(core.MonitoringModuleStore.path_store[key][2])
                                        alternative_paths_min_av_bw[key] = min( (self._store[key][1]).values() )
                                        sum_lengths += alternative_paths_lengths[key]
                if alternative_paths == []:
                        log.debug("Alternative path set = []. Abort...")
                        return
                #select flows to be moved to alternative path (decreasing first fit). 1 flow = 1 client ip
                decr_rate_flows = sorted( self._store[path_id][2], key=(self._store[path_id][2]).get)
                offloading_output = {} #result of the offloading algorithm
                for path in alternative_paths:
                        #the increment is inversely proportional to the path length (number of hops)
                        path_specific_increment = ((exceeding_bw) * (1.0/alternative_paths_lengths[path])) / (1.0/sum_lengths)
                        log.info("Path %s: +%f", path, path_specific_increment)
                        increment = 0.0
                        offloading_output[path] = []
                        for flow in decr_rate_flows:
                                if self._store[path_id][2][flow][0] > 0.0:
                                        if (increment + self._store[path_id][2][flow][0]) > path_specific_increment or (increment + self._store[path_id][2][flow][0]) > alternative_paths_min_av_bw[path]:
                                                break
                                        else:
                                                increment += self._store[path_id][2][flow][0]
                                                offloading_output[path].append(flow)
                log.info("OFFLOADING OUTPUT (alternative_path : [flow/client_ip]): ")
                print offloading_output
                #call reconfiguration enforcement procedure
                self.enforce_offload(path_id, offloading_output)



        def weight_offload(self, path_id, available_bw, congestion):
                log.info("PATH %s : OFFLOADING  (policy = 3)", path_id)
                #traffic decrement (congestion = num. paths on congested link + 1, 0 if no congestion)
                exceeding_bw = ((const.LINK_CAPACITY - available_bw) - self._cong_threshold) / (congestion - 1)
                log.info("Path %s: -%f", path_id, exceeding_bw)
                #find alternative path candidates (+ min available bw) and compute traffic shifts
                alternative_paths = []
                alternative_paths_min_av_bw = {}
                sum_min_av_bw = 0.0
                server = core.MonitoringModuleStore.path_store[path_id][1]
		ingress = core.MonitoringModuleStore.path_store[path_id][0]
		for key, entry in core.MonitoringModuleStore.path_store.iteritems():
                        if key != path_id and entry[1] == server and entry[0] == ingress:
                                #check if the candidate path can accept traffic
                                if self._store[key][0] == AVAILABLE:
                                        alternative_paths.append(key)
                                        alternative_paths_min_av_bw[key] = min( (self._store[key][1]).values() )
                                        sum_min_av_bw += alternative_paths_min_av_bw[key]
                if alternative_paths == []:
                        log.debug("Alternative path set = []. Abort...")
                        return
                #select flows to be moved to alternative path (decreasing first fit). 1 flow = 1 client ip
                decr_rate_flows = sorted( self._store[path_id][2], key=(self._store[path_id][2]).get)
                offloading_output = {} #result of the offloading algorithm
                for path in alternative_paths:
                        #the increment is proportional to the path available bandwidth
                        path_specific_increment = ((exceeding_bw) * (alternative_paths_min_av_bw[path])) / sum_min_av_bw
                        log.info("Path %s: +%f", path, path_specific_increment)
                        increment = 0.0
                        offloading_output[path] = []
                        for flow in decr_rate_flows:
                                if self._store[path_id][2][flow][0] > 0.0:
                                        #stop when the increment is "filled" or when exceeding the available bw. of alternative paths
                                        if (increment + self._store[path_id][2][flow][0]) > path_specific_increment or (increment + self._store[path_id][2][flow][0]) > alternative_paths_min_av_bw[path]:
                                                break
                                        else:
                                                increment += self._store[path_id][2][flow][0]
                                                offloading_output[path].append(flow)
                log.info("OFFLOADING OUTPUT (alternative_path : [flow/client_ip]): ")
                print offloading_output
                #call reconfiguration enforcement procedure
                self.enforce_offload(path_id, offloading_output)


        """
        Offloading enforcement (through Pox controller primitives)
        """
        def enforce_offload(self, path_id, reconfig_table):
                edge_switch = core.MonitoringModuleStore.path_store[path_id][0]
		#print "core.MonitoringModuleStore.path_store[path_id]", core.MonitoringModuleStore.path_store[path_id]
		edge_switch_server = core.MonitoringModuleStore.path_store[path_id][-1]
                server_ip = core.MonitoringModuleStore.path_store[path_id][1]
                for altern_path in reconfig_table:
                        new_tos = core.MonitoringModuleStore.path_store[altern_path][3]
                        new_output_port = self.find_new_output_port(edge_switch, altern_path)
                        for client_ip in reconfig_table[altern_path]:
                                msg = of.ofp_flow_mod()
                                msg.idle_timeout = 0.0
                                msg.hard_timeout = 0.0
                                msg.buffer_id = None
                                msg.match.dl_type = ethernet.IP_TYPE
                                msg.match.nw_src = IPAddr(client_ip)
                                msg.match.nw_dst = IPAddr(server_ip)
                                msg.actions.append(of.ofp_action_nw_tos(nw_tos = new_tos))
                                msg.actions.append(of.ofp_action_output(port = new_output_port))
                                core.openflow.connections[int(edge_switch[1:])].send(msg)
                                log.info("OFFLOADING %s: New rule set on %s for client %s", path_id, edge_switch, client_ip)
                                #(flows become ghosts until next flow-stats are extracted, is it a problem or not?:

                                #delete flow from the local store (it will be automatically re-inserted for the new path)
                                del(self._store[path_id][2][client_ip])

		for altern_path in reconfig_table:
                        new_tos = core.MonitoringModuleStore.path_store[altern_path][3]
                        new_output_port = self.find_new_output_port_server(edge_switch_server, altern_path)
                        #core.UdpIf.send..(server_sw, server_id, new_tos, new_out_port)
			for client_ip in reconfig_table[altern_path]:
                                msg = of.ofp_flow_mod()
                                msg.idle_timeout = 0.0
                                msg.hard_timeout = 0.0
                                msg.buffer_id = None
                                msg.match.dl_type = ethernet.IP_TYPE
                                msg.match.nw_dst = IPAddr(client_ip)
                                msg.match.nw_src = IPAddr(server_ip)
                                msg.actions.append(of.ofp_action_nw_tos(nw_tos = new_tos))
                                msg.actions.append(of.ofp_action_output(port = new_output_port))

				# if under control then not send, else send
				mgr_id = None
				try:
					f_mgr = open ('/home/mininet/pox/config/mgr_clusters.txt','r')
				except Exception as exc2:
					print "Error accessing mgr_clusters.txt: ", exc2
                		for line in f_mgr:

				        line = line[:-1]
				        temp_line = line
				        temp_line = temp_line.split('|')
				        temp_line[1] = temp_line[1].split(';')
				        line = [temp_line[0]]+temp_line[1]
        				line[0] = 's'+line[0]

					temp_edge_switch_server = str(edge_switch_server)
					temp_edge_switch_server = temp_edge_switch_server[1:]
                       			if temp_edge_switch_server in line:
                                                mgr_id = str(line[0])
                                                mgr_id = int(mgr_id[1:])
						# If target server switch under control, reconfig directly
                               			if mgr_id == self.mgr:
							log.info("This switch is under control!")
							core.openflow.connections[int(edge_switch_server[1:])].send(msg)
							log.info("OFFLOADING %s: New rule set on %s for client %s", path_id, edge_switch_server, client_ip)
							log.info("Direct reconfig successed")
                                       			break
						else: 
                                                        log.info("This switch is remote!")
							core.UDPInterface.send_path(int(edge_switch_server[1:]),edge_switch_server,client_ip,server_ip,new_tos,new_output_port,mgr_id,path_id)
							break
               			if mgr_id == None:
                       			log.info("BUG: No LM available!!")
                       			return
 
				#core.UDPInterface.send_path(int(edge_switch_server[1:]),edge_switch_server,client_ip, server_ip, new_tos, new_out_port)
                                #core.openflow.connections[int(edge_switch_server[1:])].send(msg)
                                #log.info("OFFLOADING %s: New rule set on %s for client %s", path_id, edge_switch_server, client_ip)
                                #(flows become ghosts until next flow-stats are extracted, is it a problem or not?:

                                #delete flow from the local store (it will be automatically re-inserted for the new path)
                                #del(self._store[path_id][2][client_ip])

        def find_new_output_port(self, edge_switch, new_path):
                first_link = core.MonitoringModuleStore.path_store[new_path][2][0]
                if core.MonitoringModuleStore.topo_store[first_link][0] == edge_switch:
                        out_port = core.MonitoringModuleStore.topo_store[first_link][1]
                if core.MonitoringModuleStore.topo_store[first_link][2] == edge_switch:
                        out_port = core.MonitoringModuleStore.topo_store[first_link][3]
                return int(out_port[3:])


	def find_new_output_port_server(self, edge_switch, new_path):
                last_link = core.MonitoringModuleStore.path_store[new_path][2][-1]
                #print "last link", last_link
		#print "edge switch", edge_switch
		if core.MonitoringModuleStore.topo_store[last_link][0] == edge_switch:
                        out_port = core.MonitoringModuleStore.topo_store[last_link][1]
                if core.MonitoringModuleStore.topo_store[last_link][2] == edge_switch:
                        out_port = core.MonitoringModuleStore.topo_store[last_link][3]
                return int(out_port[3:])

        """
        """


def launch(manager = DEFAULT_MGR, offloading = EQ_OFFLOAD):
        core.registerNew(AppPathSelection, int(manager), int(offloading))
