from pox.core import core
from pox.lib.recoco import Timer
from datetime import datetime
#library imported for offloading decision enforcement
import pox.openflow.libopenflow_01 as of
import sys
import const
import fileinput
import random

DEFAULT_MGR = 0
RESTORE_DEFAULT_TIMEOUT = const.RESTORE_TIMEOUT
PATH_BACKOFF = 5.0

log = core.getLogger()

class AppRedirect(object):
        def __init__(self, manager):
                self.mgr = manager
                self.conf_period = 10.0 #10 seconds
                self._store = {}  #dict <server>
                self._mapping = {} #dict <client, list(server)>
                self._current_mapping = {} #dict <client, server>
		self._aux_client_ip = {}
                self._cong_threshold = const.LINK_CAPACITY * 0.7
                self.load_init_mapping()
                self.init_store()
                #periodically restore mapping if no congestion
                #Timer(RESTORE_DEFAULT_TIMEOUT, self.restore_default, args=[], recurring=True)

        def load_init_mapping(self):
                try:
                        f_mapping = open("./config/mapping.txt", 'r')
            	except:
                        print "Error: While opening the mapping file"
                for line in f_mapping:
                        line = line[0:len(line)-1].split(" ")
                        client = (line[0].split(':'))[0]
			#does the client belong to this cluster?
			full_client_name = 'client' + client
			for key,link in core.MonitoringModuleStore.topo_store.iteritems():
				if link[0] == full_client_name:
					sw = link[2]
					break
				if link[2] == full_client_name:
					sw = link[0]
					break
			if sw not in core.MonitoringModuleStore.sw_cluster:
				continue	
		
                        self._mapping[client] = []
                        for s in line[1:len(line)]:
                                self._mapping[client].append(s)
                        self._current_mapping[client] = line[1]
                log.debug("Client/Server mapping list:")
                print self._mapping
		log.debug("Current client/server mapping")
		print self._current_mapping

        def init_store(self):
                try:
                        f_path_client = open("./config/path_client.txt", 'r')
                except:
                        print "Error: While opening the path-client file"
                for line in f_path_client:
                        line = line[0:len(line)-1].split(" ")
                        path_id = line[0]
                        if 'P'+path_id not in self._store:
                                self._store['P'+path_id] = ["AVAILABLE", const.LINK_CAPACITY, [line[2]]]
                        else:
                                self._store['P'+path_id][2].append(line[2])
			self._aux_client_ip[line[2]] = line[1][6:]
                log.debug("Initial Path Store")
		print self._store

		log.info("Client IP datastore")
		print self._aux_client_ip



        # def find_clients_on_path(self, path_id):
        #         clients_on_path = []
        #         try:
        #     	       f_path_client = open("./config/path_client.txt", 'r')
        #     	except:
        #                 sys.exit("Error: While opening the path_client file")
        #         for line in f_path_client:
        #                 line = line.split(" ")
        #                 if line[0] == path_id: #check IP
        #                         clients_on_path.append(line[1])
        #         return clients_on_path





        #handle link update (one update per link)
        def handle_update_from_app_cong_det(self, link_id, available_tx_mbps, available_rx_mbps, time, congestion):
		"""
                log.info("Update received by the Congestion Detection App. for link %s", link_id)
                #find what are the paths associated to the link
                for path_id in core.MonitoringModuleStore.path_store:
                        if link_id in core.MonitoringModuleStore.path_store[path_id][2]:
                                #adapt store
                                self._store[path_id][1]= min(available_tx_mbps, available_rx_mbps)
                                if congestion>0 and self._store[path_id][0] == "AVAILABLE":
					#log.info("Calling mapping update procedure...")
                                        self._store[path_id][0] = "OFFLOADING"
                                        Timer(PATH_BACKOFF, self.unlock_path, args=[path_id], recurring=False)
                                        for client_ip in self._store[path_id][2]:
						client = self._aux_client_ip[client_ip]
						if client in self._mapping: 
                                                	self.adapt_mapping(path_id, client)
		"""
		return

        def unlock_path(self,path_id):
                self._store[path_id][0] = "AVAILABLE"
	
	"""
	TODO: only the manager of the corresponding cluster should take reconf.
	"""
        
	def adapt_mapping_from_path_sel(self, client_ip):
		client = self._aux_client_ip[client_ip]
		if client not in self._mapping:
			log.info("No mapping entry for client %s", client)
			return
                log.info("Adapting server selection (CALLED BY PATH SELECT) for client %s", client)
                new_selected_server = None
                #search alternative paths
                server_list = self._mapping[client]
                print server_list
                for i in range(0, len(server_list)-1):
                        if server_list[i] == self._current_mapping[client]:
                                new_selected_server = server_list[i+1]
                                break
                if new_selected_server!=None:
                        #set restore timeout: 10 secs.
			timeout = random.uniform(5, 20) #min = 5, max = 20
                        Timer(timeout, self.restore_default, args=[client], recurring=False)
                        log.info("New server selection for client %s: %s", client, new_selected_server)
                        self._current_mapping[client] = new_selected_server
                        """
                        #update mapping configuration file
                        for line in fileinput.input('./config/mapping.txt', inplace=True):
                                print line
                                print line.split(' ')[0].split(':')[0]
                                if line.split(' ')[0].split(':')[0] == client:
                                        print "client"
                                #splitted_line = line[0:len(line)-1].split(" ")
                                #if (splitted_line[0].split(':'))[0] == client:
                                        print line.rstrip().replace(line, '1:10.0.0.7 10.0.0.6 10.0.0.7\n')
                                        break
                        """
                        #update mapping configuration file
                        new_line = client + ':' + new_selected_server
                        for server_ip in self._mapping[client]:
                                new_line += (' ' + server_ip)
                        new_line += '\n'
                        replace_line("./config/mapping.txt", int(client)-1, new_line)

	def adapt_mapping(self, path_id, client):
		log.info("Adapting server selection for client %s", client)
                new_selected_server = None
                #search alternative paths
                server_list = self._mapping[client]
		print server_list
                for i in range(0, len(server_list)-1):
                        if server_list[i] == self._current_mapping[client]:
                                new_selected_server = server_list[i+1]
                                break
                if new_selected_server!=None:
			#set restore timeout: 10 secs.
			timeout = random.uniform(5, 20) #min = 5, max = 20
			Timer(timeout, self.restore_default, args=[client], recurring=False)	
                        log.info("New server selection for client %s: %s", client, new_selected_server)
			self._current_mapping[client] = new_selected_server
			"""
			#update mapping configuration file
			for line in fileinput.input('./config/mapping.txt', inplace=True):
				print line
				print line.split(' ')[0].split(':')[0]
				if line.split(' ')[0].split(':')[0] == client:
					print "client"
				#splitted_line = line[0:len(line)-1].split(" ")
				#if (splitted_line[0].split(':'))[0] == client:
					print line.rstrip().replace(line, '1:10.0.0.7 10.0.0.6 10.0.0.7\n')
				        break
			"""
			#update mapping configuration file
			new_line = client + ':' + new_selected_server
			for server_ip in self._mapping[client]:
				new_line += (' ' + server_ip)
			new_line += '\n'
			replace_line("./config/mapping.txt", int(client)-1, new_line)


	def restore_default(self, client_id):
		for client in self._current_mapping:
			if client == client_id:
				log.info("Restoring default mapping for client %s", client)
				self._current_mapping[client] = self._mapping[client][0]
				#update mapping configuration file
                        	new_line = client + ':' + self._current_mapping[client]
                        	for server_ip in self._mapping[client]:
                                	new_line += (' ' + server_ip)
                        	new_line += '\n'
                        	replace_line("./config/mapping.txt", int(client)-1, new_line)


def replace_line(file_name, line_num, text):
	lines = open(file_name, 'r').readlines()
        lines[line_num] = text
        out = open(file_name, 'w')
        out.writelines(lines)
        out.close()

def launch(manager = DEFAULT_MGR):
        core.registerNew(AppRedirect, int(manager))

