from pox.core import core

DEFAULT_MGR = 0

log = core.getLogger()

class MonitoringModuleRequirementInterpreter(object):
        def __init__(self, manager):
                self.mgr = manager
                log.debug("Initializing Monitoring Module Requirement Interpreter...")

                #App-Requirements data structre
                self.app_requirements = {}
                #Low-Level Targets data structure
                self.low_level_targets = {}



        #Called by the app. instances
        def register_req(self, req):
                new_requirement_key = len(self.app_requirements) + 1 #start from key=1
                self.app_requirements[new_requirement_key] = req
                print self.app_requirements
                #start requirement processing
                self.handle_high_level_requirement(new_requirement_key, req)



        #Analyse high-level requirement: Target? Intent?
        def handle_high_level_requirement(self, new_req_key, req):
                #what kind of high-level target?
                if req[1][0] == "LINK":
                        log.debug("New requirement received from %s, Target Type: %s-%s", req[0], req[1][0], req[1][1])
                        if req[1][1] == "UTIL":
                                self.translate_link_util(new_req_key, req[0], req[2], req[1][2], req[3])
                        else:
                                log.debug("Only UTIL intent is supported!")
                elif req[1][0] == "PATH":
                        log.debug("New requirement received from %s, Target Type: %s-%s", req[0], req[1][0], req[1][1])
                        if req[1][1] == "UTIL":
                                self.translate_path_util(new_req_key, req[0], req[2], req[1][2], req[3])
                        else:
                                log.debug("Only UTIL intent is supported!")
                else:
                        log.debug("Only PATH and LINK targets type are supported!")



        #Translate LINK,UTIL requirement into low-level targets (PORT:RATE)  #FIXME The frequency should be given per each link!
        def translate_link_util(self, new_req_key, app_id, link_list, aggr_level, required_meas_frequency):
                if aggr_level != "":
                        log.debug("The only Aggregation supported is the default one: individual link")
                        return
                to_sched = [] #list of low-level-target to be sent to scheduler for scheduling
                to_resched = [] #list of low-level-target to be sent to scheduler for re-scheduling
                #Find corresponding switch ports (select only first pair (switch,port) in topo_store)
                for link in link_list:
			#can only poll the switches inside the local cluster
                        link_spec = core.MonitoringModuleStore.topo_store[link]
                        if "client" not in link_spec[0] and "server" not in link_spec[0] and link_spec[0] in core.MonitoringModuleStore.sw_cluster:
				switch = link_spec[0]
				port = link_spec[1]
			else:
				switch = link_spec[2]
				port = link_spec[3]
                        low_level_resource = switch + ':' + port

                        #Register low-level target (or update an existing one): [low_level_id:sw,port]:[type=PORT, high_level_target, period(freq), App_req, instructions_for_scheduler]
                        #check if a similar target is already registered #TODO This should go on separate method
                        similar_entry_found_flag = False
                        for target_id in self.low_level_targets:
                                if target_id == low_level_resource and self.low_level_targets[target_id][0] == 'PORT:RATE':
                                        log.debug("Similar low-level target found!")
                                        #low-level target already exists. Check measurement frequency.
                                        similar_entry_found_flag = True
                                        if self.low_level_targets[target_id][2] < required_meas_frequency:
                                                #update existing entry: keep previous period (frequency)
                                                self.low_level_targets[target_id][3].append(new_req_key)
                                        else:
                                                #update existing entry: update period(frequency) :(new scheduling necessary)
                                                self.low_level_targets[target_id][2] = required_meas_frequency
                                                self.low_level_targets[target_id][3].append(new_req_key)
                                                self.low_level_targets[target_id][4] = "RE-SCHEDULE-IT"
                                                to_resched.append(low_level_resource)
                                        break

                        if similar_entry_found_flag == False:
                                self.low_level_targets[switch + ":" + port] = ["PORT:RATE", [link], required_meas_frequency, [new_req_key], "SCHEDULE-IT"]
                                to_sched.append(low_level_resource)
                #Test
                print "LOW LEVEL TARGETS:"
                print self.low_level_targets
                print "TO BE SCHEDULED:"
                print to_sched
                print "TO BE RESCHEDULED:"
                print to_resched

                # Send to the scheduler (Type of measurement ops: PORT-POLL)
                core.MonitoringModuleScheduler.schedule(to_sched, to_resched, "PORT:RATE")



        def translate_path_util(self, new_req_key, app_id, path_list, aggr_level, required_meas_frequency):
                if aggr_level != "flow":
                        log.debug("The only Aggregation level supported is flow level")
                        return
                to_sched = [] #list of low-level-target to be sent to scheduler for scheduling
                to_resched = [] #list of low-level-target to be sent to scheduler for re-scheduling
                for path_id in path_list:
                        #select path edge switch and path_code (which corresponds to tos field on datapath)
                        edge_switch =  core.MonitoringModuleStore.path_store[path_id][0]
                        server = core.MonitoringModuleStore.path_store[path_id][1]
                        path_code = str(core.MonitoringModuleStore.path_store[path_id][3])
                        low_level_resource = edge_switch + ':' + server + ':' + path_code
                        #check for similar entries in the low_level_targets table
                        similar_entry_found_flag = False
                        for target_id in self.low_level_targets:
                                if target_id == low_level_resource and self.low_level_targets[target_id][0] == 'FLOW:RATE':
                                        log.debug("Similar low-level target found!")
                                        #low-level target already exists. Check measurement frequency.
                                        similar_entry_found_flag = True
                                        if self.low_level_targets[target_id][2] < required_meas_frequency:
                                                #update existing entry: keep previous period (frequency)
                                                self.low_level_targets[target_id][3].append(new_req_key)
                                        else:
                                                #update existing entry: update period(frequency) :(new scheduling necessary)
                                                self.low_level_targets[target_id][2] = required_meas_frequency
                                                self.low_level_targets[target_id][3].append(new_req_key)
                                                self.low_level_targets[target_id][4] = "RE-SCHEDULE-IT"
                                                to_resched.append(low_level_resource)
                                        break
                        if similar_entry_found_flag == False:
                                self.low_level_targets[low_level_resource] = ["FLOW:RATE", [path_id], required_meas_frequency, [new_req_key], "SCHEDULE-IT"]
                                to_sched.append(low_level_resource)
                #Test
                print "LOW LEVEL TARGETS:"
                print self.low_level_targets
                print "TO BE SCHEDULED:"
                print to_sched
                print "TO BE RESCHEDULED:"
                print to_resched
                #Send to the scheduler (Type of measurement ops: FLOW-POLL)
                core.MonitoringModuleScheduler.schedule(to_sched, to_resched, "FLOW:RATE")



        """
        DEPRECATED
        """
        # def translate_path_util(self, new_req_key, app_id, path_list, aggr_level, required_meas_frequency):
        #         if aggr_level != "flow":
        #                 log.debug("The only Aggregation level supported is flow level")
        #                 return
        #         to_sched = [] #list of low-level-target to be sent to scheduler for scheduling
        #         to_resched = [] #list of low-level-target to be sent to scheduler for re-scheduling
        #         for path_id in path_list:
        #                 #look flows on this path and add them to low_level_targets table
        #                 for flow_id in core.MonitoringModuleStore.flow_store:
        #                         if path_id == core.MonitoringModuleStore.flow_store[flow_id][2]:
        #                                 edge_switch =  core.MonitoringModuleStore.flow_store[flow_id][1]
        #                                 low_level_resource = edge_switch + ':' + flow_id
        #                                 #check for similar entries in the low_level_targets table
        #                                 similar_entry_found_flag = False
        #                                 for target_id in self.low_level_targets:
        #                                         if target_id == low_level_resource and self.low_level_targets[target_id][0] == 'FLOW:RATE':
        #                                                 log.debug("Similar low-level target found!")
        #                                                 #low-level target already exists. Check measurement frequency.
        #                                                 similar_entry_found_flag = True
        #                                                 if self.low_level_targets[target_id][2] < required_meas_frequency:
        #                                                         #update existing entry: keep previous period (frequency)
        #                                                         self.low_level_targets[target_id][3].append(new_req_key)
        #                                                 else:
        #                                                         #update existing entry: update period(frequency) :(new scheduling necessary)
        #                                                         self.low_level_targets[target_id][2] = required_meas_frequency
        #                                                         self.low_level_targets[target_id][3].append(new_req_key)
        #                                                         self.low_level_targets[target_id][4] = "RE-SCHEDULE-IT"
        #                                                         to_resched.append(low_level_resource)
        #                                                 break
        #                                 if similar_entry_found_flag == False:
        #                                         self.low_level_targets[low_level_resource] = ["FLOW:RATE", [path_id], required_meas_frequency, [new_req_key], "SCHEDULE-IT"]
        #                                         to_sched.append(low_level_resource)
        #         #Test
        #         print "LOW LEVEL TARGETS:"
        #         print self.low_level_targets
        #         print "TO BE SCHEDULED:"
        #         print to_sched
        #         print "TO BE RESCHEDULED:"
        #         print to_resched
        #         # Send to the scheduler (Type of measurement ops: FLOW-POLL)
        #         core.MonitoringModuleScheduler.schedule(to_sched, to_resched, "FLOW:RATE")



def launch (manager = DEFAULT_MGR):
	   core.registerNew(MonitoringModuleRequirementInterpreter, int(manager))
