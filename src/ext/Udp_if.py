# UDP-Socket-based interface between 2 Managers

import socket
import sys
from pox.core import core
from datetime import datetime
from threading import Thread
from pox.lib.recoco import Timer
import pox.openflow.libopenflow_01 as of
from pox.lib.packet.ethernet import ethernet
from pox.lib.addresses import IPAddr, EthAddr

DEFAULT_MGR = 0
BUFFER_SIZE = 1000 # We define a 1000 bytes default udp buffer size
log = core.getLogger()

class UDPInterface:
	def __init__(self, manager):
		self.mgr = manager
		self.port = None
		self.ip = None
		self.mgrs_store = {} #store managers ip/ports
		self.init_mgrs_store()
		#start udp socket
        	self.Sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        	self.Sock.bind( (self.ip, self.port) )
            	#start the listening thread
        	self.listener_thread = Thread(target = self.udp_rx).start()

		"""
		DEPRECATED
		# Test : Send a test msg to the remote peer (i.e. the remote UDPInterface)
		#Timer(10, self.udp_tx, args = ["New test message"], recurring=False)
		"""


	def init_mgrs_store(self):
		try:
			mgr_f = open("./config/mgr_ip_port.txt", 'r')
		except:
			sys.exit("Error: While opening the switch-cluster file")
		for line in mgr_f:
			line = line.split(' ')
			self.mgrs_store[line[0]] = [line[1], int(line[2])] # e.g. 1:[127.0.0.1, 6634]
			if int(line[0]) == self.mgr:
				self.port = int(line[2])
				self.ip = line[1]
		print "MANAGER, IP, PORT:"
		print self.mgrs_store


	#def send...
		#create string JASON||server_sw||server_id||new_tos||output_port

	#core.UDPInterface.send_path(int(edge_switch_server[1:]),edge_switch_server,client_ip, server_ip, new_tos, new_out_port, mgr_id)
	def send_path(self,OF_connection,edge_switch_server,client_ip, server_ip, new_tos, new_out_port, mgr_id, path_id):
		# Construct msg to send
		msg = "PA_"+str(OF_connection)+","+str(edge_switch_server)+","+str(client_ip)+","+str(server_ip)+","+str(new_tos)+","+str(new_out_port)+","+ str(mgr_id)+","+str(path_id)+";"
		'''
		print "&&&&&&&&&&"
		print "msg ready to send, (int(edge_switch_server[1:]),edge_switch_server,client_ip, server_ip, new_tos, new_out_port, mgr_id)"
		print msg
		print "&&&&&&&&&&"
		'''
		# Find mgr_id
		'''
		mgr_id = None
		f_mgr = open ("/home/mininet/pox/config/mgr_clusters.txt",'r')
		for line in f_mgr:
			line = line.split(" ")
			if str(edge_switch_server) in line:
				mgr_id = str(line[0])
				mgr_id = int(mgr_id[3:])
				if mgr_id != self.mgr:
					break
		if mgr_id == None:
			print "No LM available"
			return	
		'''
		# Find remote mgr ip, port, latency and send
		mgr_id = str(mgr_id)
		rem_ip = self.mgrs_store[mgr_id][0]
                rem_port = self.mgrs_store[mgr_id][1]
		latency = core.MonitoringModuleStore.mgr_lat['s' + mgr_id] * 0.001
		Timer(latency, self.Sock.sendto, args=[msg, (rem_ip, rem_port)], recurring=False)
		log.info("UDP Interface Sent")

	#def receive
	def udp_rx(self):
		log.info ("Waiting for messages")
        	while 1:
			try:
            			msg, (rem_addr, rem_port) = self.Sock.recvfrom( BUFFER_SIZE )
            			#log.debug("Msg received from %s:%s\nMsg: %s", str(rem_addr),str(rem_port), msg)
				if msg == "EXIT" :   # If the received msg is "EXIT", the thread stops
					break
				if msg[0:2] == "CD":
					#find the remote mgr who sent the msg
					for mgr_id in self.mgrs_store:
						 if self.mgrs_store[mgr_id][0] == rem_addr and self.mgrs_store[mgr_id][1] == rem_port:
						 	#send update to the local Congestion Detection app. instance
						 	log.info("Message received from app CD_%s", mgr_id)
		 				 	core.AppCongestionDetection.handle_msg_from_udp_if(msg, "CD_"+mgr_id)
				if msg[0:2] == "PA":
					log.info( "Remote msg received")
                                        msg = msg[3:]
                                        msg = msg.split(',')
					msg[0] = int(msg[0])
                                	msg_OF = of.ofp_flow_mod()
                               		msg_OF.idle_timeout = 0.0
                                	msg_OF.hard_timeout = 0.0
                                	msg_OF.buffer_id = None
                                	msg_OF.match.dl_type = ethernet.IP_TYPE
                                	msg_OF.match.nw_dst = IPAddr(msg[2])
                                	msg_OF.match.nw_src = IPAddr(msg[3])
                                	msg_OF.actions.append(of.ofp_action_nw_tos(nw_tos = int(msg[4])))
                                	msg_OF.actions.append(of.ofp_action_output(port = int(msg[5])))
                                	core.openflow.connections[msg[0]].send(msg_OF)
                                	log.info("OFFLOADING %s: New rule set on %s for client %s", msg[7], msg[1], msg[2])

				else:
					log.debug("Only Messages to/from Congestion Detection App. Instances are supported")
			except KeyboardInterrupt:
				self.sock.close()
				break



	def udp_tx(self, msg, rem_app_list):
		for app_id in rem_app_list:
			mgr_id = app_id[3:]
			rem_ip = self.mgrs_store[mgr_id][0]
			rem_port = self.mgrs_store[mgr_id][1]
			#Send data for each app instance
			#fetch the latency (to the remote manager)
			latency = core.MonitoringModuleStore.mgr_lat['s' + mgr_id] * 0.001 #from ms to s
			#print str(datetime.now().time()) + " :to " + mgr_id + " " + str(latency)
			Timer(latency, self.Sock.sendto, args=[msg, (rem_ip, rem_port)], recurring=False)
        		#self.Sock.sendto( msg, (rem_ip, rem_port) )


def launch (manager = DEFAULT_MGR):
	core.registerNew(UDPInterface, int(manager))
