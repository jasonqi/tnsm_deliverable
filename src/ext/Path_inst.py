from pox.core import core
from pox.lib.util import dpid_to_str
from pox.lib.packet.ethernet import ethernet
import pox.openflow.libopenflow_01 as of
from pox.lib.addresses import IPAddr
import sys
import re

PATH_TOS = [32, 40, 48, 56, 64, 72, 80, 88] # 8 tos values = 8 possible paths per pair (source, sink)
DEFAULT_MGR = 0

log = core.getLogger()

class PathInstaller (object):

	def __init__ (self, manager):
		self.mgr = manager
		self.topo_l = [] #list [ [switch1][if1][switch2][if2] ]
		self.sp_l = {} #dict {path_key, shortest_path_nodes, server}
		self.cl_d = {} #dict {client,path to reach the server}
		self.aux_ip_cl = {} #auxiliary {ip, client} table
		self.aux_servers = {} # {dest_switchm, servername, ip} table
		self.load_aux_servers()
		self.load_sp_l()
		self.load_topo_l()
		self.load_cl_d()
		self.init_mon = 0 #start monitoring when first switch connect
		core.openflow.addListeners(self)

	def load_aux_servers(self):
	 	log.info("Loading (auxiliary) servers data structure")
	 	#open links, interfaces file
	 	try:
	 		server_f = open("./config/servers.txt", 'r')
	 	except:
	 		sys.exit("Error: While opening the servers file")
			return 1
	 	for line in server_f:
	 		line=line[:len(line)-1] #delete final \n
	 		line=line.split(" ")
			#self.aux_servers[line[1]] = [line[0], line[2]]
	 		self.aux_servers[line[2]] = [line[0], line[1]]
		print '@@@@@@@@@@!!!!!!!!!!!!!'
		print self.aux_servers
		print '@@@@@@@@@@!!!!!!!!!!!!!'

	'''
        def load_sp_l(self):
		log.info("Loading shortest paths...")
                #open shortest paths file
                try:
                        sp_f = open("./config/sp_output.txt", 'r')
                except:
                        sys.exit("Error: While opening the shortest paths file")
                        return 1

                #load the shortest paths (source, sink) list
                path_key = 1
                aux_tos = [] #store already used ToS numbers.
		for server_ip in self.aux_servers:
			print 'xxxxxxx'
			print server_ip
			print self.aux_servers[server_ip]
                	try:
                        	sp_f = open("./config/sp_output.txt", 'r')
                	except:
                        	sys.exit("Error: While opening the shortest paths file")
                        	return 1
			for line in sp_f:
				line = line[:len(line)-1] #delete final \n
				parsed_line = []
				line=line.split(" ")
				#print self.aux_servers[str(server_ip)][1]
				temp = 's' + line[len(line) -1]
				if temp != str(self.aux_servers[str(server_ip)][1]):
					print temp 
					print str(self.aux_servers[str(server_ip)][1])
					print "============NOT EQUAL++++++++++"
					continue
				print temp
                                print str(self.aux_servers[str(server_ip)][1])
                                print "============EQUAL++++++++++"
				path_tos = None
				for tos in PATH_TOS:
					if [line[0], server_ip, tos] not in aux_tos:
						path_tos = tos
						aux_tos.append([line[0], server_ip, tos])
						break
				if path_tos == None:
					sys.exit("Error. Most likely raised because not enough ToS values")
				self.sp_l[path_key] = [line, server_ip, path_tos]
                        	self.cl_d[str(path_key)] = []
                        	path_key +=1
			sp_f.close()
		print ']]]]]]]]]]]]]]]'
		print self.sp_l
		print '[[[[[[[[[[[[[['
	'''
	def load_sp_l(self):
		log.info("Loading shortest paths...")
		#open shortest paths file
		try:
			sp_f = open("./config/sp_output.txt", 'r')
		except:
			sys.exit("Error: While opening the shortest paths file")
			return 1
		#load the shortest paths (source, sink) list
		path_key = 1
		aux_tos = [] #store already used ToS numbers.
		for line in sp_f:
			line = line[:len(line)-1] #delete final \n
			parsed_line = []
			line=line.split(" ")
			server_ip = self.aux_servers['s' + line[len(line) -1]][1]
			#find available TOS
			path_tos = None
			for tos in PATH_TOS:
				if [line[0], server_ip, tos] not in aux_tos:
					path_tos = tos
					aux_tos.append([line[0], server_ip, tos])
					break
			if path_tos == None:
				sys.exit("Error. Most likely raised because not enough ToS values")
			self.sp_l[path_key] = [line, server_ip, path_tos]
			self.cl_d[str(path_key)] = []
			path_key +=1
		print self.sp_l
		#open output installed_path.txt file
		try:
			out_f = open("./config/mgr" + str(self.mgr) + "_installed_path.txt", 'w')
		except:
			sys.exit("Error: While opening output file")
			return 1
		for path_key in self.sp_l:
			out_f.write(str(path_key) + " ")
			for hop in self.sp_l[path_key][0]:
				out_f.write(hop + ",")
			out_f.write(" " + self.sp_l[path_key][1] + " " + str(self.sp_l[path_key][2]) + '\n')
		out_f.close()


	def load_topo_l(self):
		log.info("Loading links in format [s_x eth_i s_y eth_j]...")
		#open links, interfaces file
		try:
			topo_f = open("./config/out_links_if.txt", 'r')
		except:
			sys.exit("Error: While opening the links-interface file")
			return 1

		#load the links-interface list
		for line in topo_f:
			line = line[:len(line)-1] #delete final \n
			parsed_line = []
			line=line.split(" ")
			for i in range(1,len(line)):
				parsed_line.append(line[i])
			self.topo_l.append(parsed_line)
		print self.topo_l


	def load_cl_d(self):
		log.info("Loading paths-clients data structure...")
		#open links, interfaces file
		try:
			client_f = open("./config/path_client.txt", 'r')
		except:
			sys.exit("Error: While opening the clients-paths file")
			return 1

		#load the links-interface list
		for line in client_f:
			line = line[:len(line)-1] #delete final \n
			line=line.split(" ")
			if line[0] not in self.cl_d:
					self.cl_d[line[0]] = [line[2]]
			else:
					self.cl_d[line[0]].append(line[2])
			self.aux_ip_cl[line[2]] = line[1] #fill auxiliary {ip,clientname} table
		print self.cl_d
		print self.aux_ip_cl



	def conf_s_edge(self, path, sw_id, event):
		log.info("Configuring src edge (node %s) for path %d ...", sw_id, path)

		#find the next hop (nh) from the (path_id)th path
		for i in range(len(self.sp_l[path][0])):
			if sw_id == self.sp_l[path][0][i]:
				nh_id = self.sp_l[path][0][i+1]
				break
		#log.info("Next hop id : %s", nh_id)

		#find the interface connected to the next hop
		for item in self.topo_l:
			if item[0] == 's'+sw_id and item[2] == 's'+nh_id:
				sw_if_str = item[1]
				nh_if_str = item[3]
			elif (item[0] == 's'+nh_id) and (item[2] == 's'+sw_id):
				sw_if_str = item[3]
				nh_if_str = item[1]
		sw_if_n = int(sw_if_str[3:])
		nh_if_n = int(nh_if_str[3:]) #may be useful...

		#install direct flow rule
		for client_ip in self.cl_d[str(path)]:
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 0
			msg.hard_timeout = 0
			msg.buffer_id = None
			msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = client_ip
			msg.match.nw_dst = self.sp_l[path][1]
			msg.actions.append(of.ofp_action_nw_tos(nw_tos = self.sp_l[path][2]))
			msg.actions.append(of.ofp_action_output(port = sw_if_n))
			event.connection.send(msg)

		for client_ip in self.cl_d[str(path)]:
			#find the interface connected to the client having ip=client_ip
			sw_if_n = None
			clientname = self.aux_ip_cl[client_ip] #e.g. client1, client2
			for item in self.topo_l:
				if item[0] == clientname and item[2] == 's'+sw_id:
					sw_if_n = int(item[3][3:])
					break
				elif item[2] == clientname and item[0] == 's'+sw_id:
					sw_if_n = int(item[1][3:]) #e.g. from 'eth3' to 3
					break
			try:
				#install reverse rule (the ToS is wildcarded)
				msg = of.ofp_flow_mod()
				msg.idle_timeout = 0
				msg.hard_timeout = 0
				msg.buffer_id = None
				msg.match.dl_type = ethernet.IP_TYPE
				msg.match.nw_src = self.sp_l[path][1]
				msg.match.nw_dst = client_ip
				msg.actions.append(of.ofp_action_output(port = sw_if_n))
				event.connection.send(msg)
			except:
				sys.exit("Problem when installing rule to client. (Interface not found)")


	def conf_d_edge(self, path, sw_id, event):
		log.info("Configuring dst edge (node %s) for path %d ...", sw_id, path)

		#find the interface connected to the next hop (in this case the next hop is the server)
		for item in self.topo_l:
			if item[0] == 's'+sw_id and 'server' in item[2]:
				sw_if_str = item[1]
				nh_if_str = item[3]
			elif 'server' in item[0] and (item[2] == 's'+sw_id):
				sw_if_str = item[3]
				nh_if_str = item[1]
		sw_if_n = int(sw_if_str[3:])
		nh_if_n = int(nh_if_str[3:])

		#install the direct rules, one per client (the ToS is wildcarded)
		for client_ip in self.cl_d[str(path)]:
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 0
			msg.hard_timeout = 0
			msg.buffer_id = None
			msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = client_ip
			msg.match.nw_dst = self.sp_l[path][1]
			msg.actions.append(of.ofp_action_output(port = sw_if_n))
			event.connection.send(msg)


		#find the previous hop (ph) from the (path_id)th path
		for i in range(len(self.sp_l[path][0])):
			if sw_id == self.sp_l[path][0][i]:
				ph_id = self.sp_l[path][0][i-1]
				break

		#find the interface connected to the previous hop
		for item in self.topo_l:
			if item[0] == 's'+sw_id and item[2] == 's'+ph_id:
				sw_ph_if_str = item[1] # if:switch-->previous hop
				ph_if_str = item[3] # if:previous hop-->switch
			elif (item[0] == 's'+ph_id) and (item[2] == 's'+sw_id):
				sw_ph_if_str = item[3]
				ph_if_str = item[1]
		sw_if_n = int(sw_ph_if_str[3:])
		ph_if_n = int(ph_if_str[3:])

		#install reverse rules
		for client_ip in self.cl_d[str(path)]:
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 0
			msg.hard_timeout = 0
			msg.buffer_id = None
			msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = self.sp_l[path][1]
			msg.match.nw_dst = client_ip
			msg.actions.append(of.ofp_action_nw_tos(nw_tos = self.sp_l[path][2]))
			msg.actions.append(of.ofp_action_output(port = sw_if_n))
			event.connection.send(msg)


	def conf_core(self, path, sw_id, event):
		log.info("Configuring core (node %s) for path %d ...", sw_id, path)

		#find the next hop (nh) from the (path_id)th path
		for i in range(len(self.sp_l[path][0])):
			if sw_id == self.sp_l[path][0][i]:
				nh_id = self.sp_l[path][0][i+1]
				break

		#find the interface connected to the next hop
		for item in self.topo_l:
			if item[0] == 's'+sw_id and item[2] == 's'+nh_id:
				sw_if_str = item[1]
				nh_if_str = item[3]
			elif (item[0] == 's'+nh_id) and (item[2] == 's'+sw_id):
				sw_if_str = item[3]
				nh_if_str = item[1]
		sw_if_n = int(sw_if_str[3:])
		nh_if_n = int(nh_if_str[3:])
		#print sw_if_n

		"""
		for client_ip in self.cl_d[str(path)]:
			#install (per-client) direct flow rule
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 0
			msg.hard_timeout = 0
			msg.buffer_id = None
			msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = client_ip
			msg.match.nw_dst = self.sp_l[path][1]
			msg.match.nw_tos = self.sp_l[path][2]
			msg.actions.append(of.ofp_action_output(port = sw_if_n))
			event.connection.send(msg)
		"""
		"""
		f_clients = open( "/home/mininet/pox/config/clients.txt", 'r')
		for line in f_clients:
        		line = line[:len(line)-1]
        		line = line.split(" ")
			msg = of.ofp_flow_mod()
                        msg.idle_timeout = 0
                        msg.hard_timeout = 0
                        msg.buffer_id = None
                        msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = line[1]
                        msg.match.nw_dst = self.sp_l[path][1]
                        msg.match.nw_tos = self.sp_l[path][2]
                        msg.actions.append(of.ofp_action_output(port = sw_if_n))
                        event.connection.send(msg)
		f_clients.close()
		"""
                msg = of.ofp_flow_mod()
                msg.idle_timeout = 0
                msg.hard_timeout = 0
                msg.buffer_id = None
                msg.match.dl_type = ethernet.IP_TYPE
                msg.match.nw_src = "10.0.0.0/24"
                msg.match.nw_dst = self.sp_l[path][1]
                msg.match.nw_tos = self.sp_l[path][2]
                msg.actions.append(of.ofp_action_output(port = sw_if_n))
                event.connection.send(msg)
	
		#find the previous hop (ph) from the (path_id)th path
		for i in range(len(self.sp_l[path][0])):
			if sw_id == self.sp_l[path][0][i]:
				ph_id = self.sp_l[path][0][i-1]
				break
		#find the interface connected to the previous hop
		for item in self.topo_l:
			if item[0] == 's'+sw_id and item[2] == 's'+ph_id:
				sw_ph_if_str = item[1] # if:switch-->previous hop
				ph_if_str = item[3] # if:previous hop-->switch
			elif (item[0] == 's'+ph_id) and (item[2] == 's'+sw_id):
				sw_ph_if_str = item[3]
				ph_if_str = item[1]
		sw_if_n = int(sw_ph_if_str[3:])
		ph_if_n = int(ph_if_str[3:])

		#install reverse flow rule
		#for client_ip in self.cl_d[str(path)]:
		"""
		f_clients = open( "/home/mininet/pox/config/clients.txt", 'r')
		for line in f_clients:
                        line = line[:len(line)-1]
                        line = line.split(" ")
			#install (per-client) revers flow rule
			msg = of.ofp_flow_mod()
			msg.idle_timeout = 0
			msg.hard_timeout = 0
			msg.buffer_id = None
			msg.match.dl_type = ethernet.IP_TYPE
			msg.match.nw_src = self.sp_l[path][1]
			msg.match.nw_dst = line[1]
			msg.match.nw_tos = self.sp_l[path][2]
			msg.actions.append(of.ofp_action_output(port = sw_if_n))
			event.connection.send(msg)
		f_clients.close()
		"""
		msg = of.ofp_flow_mod() 
                msg.idle_timeout = 0
                msg.hard_timeout = 0
                msg.buffer_id = None
                msg.match.dl_type = ethernet.IP_TYPE
                msg.match.nw_src = self.sp_l[path][1]
                msg.match.nw_dst = "10.0.0.0/24"
                msg.match.nw_tos = self.sp_l[path][2]
                msg.actions.append(of.ofp_action_output(port = sw_if_n))
                event.connection.send(msg)
	def _handle_ConnectionUp (self, event):
		log.debug("Switch %d has come up.", event.dpid)
		if self.init_mon == 0:
			self.init_mon = 1
			log.info("Starting monitoring")
			core.AppCongestionDetection.load_mon_requirement()
			if hasattr(core, 'AppPathSelection'):
				core.AppPathSelection.load_mon_requirement()

		#Start rules installation for switches on the k-shortest paths
		switch_id = str(event.dpid)
		for path in self.sp_l:
			server_ip = self.sp_l[path][1]
			n_hops = len(self.sp_l[path][0])
			if switch_id == self.sp_l[path][0][0]:
				# 1. source (client side)
				self.conf_s_edge(path, switch_id, event)
			elif switch_id == self.sp_l[path][0][n_hops-1]:
				# 2. dest (server side)
				self.conf_d_edge(path, switch_id, event)
			elif switch_id in self.sp_l[path][0][1 : n_hops-1]:
				# 3. core
				self.conf_core(path, switch_id, event)



	def _handle_PacketIn (self, event):
		log.info("A PacketIn msg has arrived from switch s%d", event.dpid)
		#print event.parse()
		#print event.parse().next.tos

def launch (manager = DEFAULT_MGR):
	core.registerNew(PathInstaller, int(manager))
