"""
Constant list
"""
REVERSE_FLOWS = True  # For Path Selection: flows are from the server to the client
CONG_THR = 0.80 # 70% of link capacity (MODIFIED FROM 0.7)
LINK_CAPACITY = 10 #Capacity is fixed: 10Mbps
RESTORE_TIMEOUT = 20.0 #Restore default server/path after 20 sec
SYNC_PERIOD = 8.0 #Link util sync period
CONG_LINK = 'L14' #only consider this link when doing offloading (HACK!)
PORT_POLL_BYTES = 268
LINK_UPDATE_BYTES = 84
