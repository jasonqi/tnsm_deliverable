from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpidToStr, dpid_to_str
from pox.openflow.of_json import *
from datetime import datetime
import const
import sys
DEFAULT_MGR = 0

log = core.getLogger()

class MonitoringModuleResultsInterpreter(object):

        def __init__ (self, manager):
                self.mgr = manager
                self.dpid_swId = {}
                self.init_dpid_swId()

                # Registering event listeners
                core.openflow.addListenerByName("FlowStatsReceived", self.handle_flow_stats)
                core.openflow.addListenerByName("PortStatsReceived", self.handle_port_stats)
		
		self.path_clients = {}
		if const.REVERSE_FLOWS == True:
			self.init_path_clients() #keep track of clients, in case of traffic from the server

	def init_path_clients(self):
                try:
                        f_pc2 = open('./config/sp_output.txt','r')
                except:
                        sys.exit("Error while opening sp_output.txt")
                i = 1;
                for line in f_pc2:
                        self.path_clients['P' + str(i)] = []
                        i = i + 1
		try:
			f_pc = open('./config/path_client.txt', 'r')
		except:
			sys.exit("Error while opening path_client.txt")
		for line in f_pc:
			line = line[0:len(line)-1].split(' ')
			if 'P'+line[0] not in self.path_clients:
				self.path_clients['P' + line[0]] = [line[2]]
			else:
				self.path_clients['P' + line[0]].append(line[2])
		log.info("Path-Clients _______________________")
		print self.path_clients

        def init_dpid_swId(self):
                log.debug("Loading dpid/sw_id store for local cluster")
                for sw in core.MonitoringModuleStore.sw_cluster:
                        sw_dpid = int(sw[1:])
                        self.dpid_swId[dpid_to_str(sw_dpid)] = sw
                print self.dpid_swId

        # Receive Port Stats
        def handle_port_stats(self, event):
                tstamp = get_timestamp()
                # Stats parsing (to JSON)
		#print '\nSize message: ' + str(sys.getsizeof(event.stats))
                switch = self.dpid_swId[dpidToStr(event.connection.dpid)]
                stats = flow_stats_to_list(event.stats) #to JSON format
		#log
		f_log = open('lat.log', 'a')
                for stat in stats:
                        port_id = "eth" + str(stat['port_no'])
                        if port_id == "eth65534":
                                continue			
                        log.debug("Stats received. Switch %s, Port %s, Time %s, Tx_bytes %d Rx_bytes %d", switch, port_id, str(tstamp.time()), stat['tx_bytes'], stat['rx_bytes'])
                        low_level_target = switch + ':' + port_id
			#log
			f_log.write(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][1][0] + " RX_MEAS " + str(datetime.now().time()) + ' LM_' + str(self.mgr)+'\n')
                        # What is the objective of monitoring ?
                        if core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][0] == "PORT:RATE":
                                #filter userful info and send to the Result Processor submodule
                                tx_bytes = stat['tx_bytes']
                                rx_bytes = stat['rx_bytes']
                                core.MonitoringModuleResultsProcessor.process_port_rate(low_level_target, tx_bytes, rx_bytes, tstamp)
                        else:
                                log.debug("Only PORT:RATE operations are allowed now!")
                                #TODO find other cases
		f_log.close()


        def handle_flow_stats(self, event):
                tstamp = get_timestamp()
                switch = self.dpid_swId[dpidToStr(event.connection.dpid)]
                log.debug("Flow stats received from %s", switch)
                #for raw_stats in event.stats:
                #        print raw_stats.match
                for target in core.MonitoringModuleRequirementInterpreter.low_level_targets:
                        if switch in target and core.MonitoringModuleRequirementInterpreter.low_level_targets[target][0] == "FLOW:RATE":
                                #match the flows against the selected low_level_target
				flows_on_match = []
				if const.REVERSE_FLOWS == False:
                                	for raw_stats in event.stats:
                                        	source_ip = str(raw_stats.match.nw_src)
                                        	dest_ip = str(raw_stats.match.nw_dst)
                                        	path_tos = None
					
                                	        for action in raw_stats.actions:
                                        	        if isinstance(action, of.ofp_action_nw_tos):
                                                	        path_tos = action.nw_tos
								break
                                        	if str(path_tos) == target.split(':')[2] and dest_ip == target.split(':')[1]:
                                                	#there is a match
                                                	flows_on_match.append([source_ip, raw_stats.byte_count, raw_stats.duration_sec, raw_stats.duration_nsec])
                                elif const.REVERSE_FLOWS == True:
					for raw_stats in event.stats:
                                                source_ip = str(raw_stats.match.nw_src)
                                                dest_ip = str(raw_stats.match.nw_dst)
                                                path_id = core.MonitoringModuleRequirementInterpreter.low_level_targets[target][1][0]
						if source_ip == target.split(':')[1] and dest_ip in self.path_clients[path_id]:
                                                        #there is a match
                                                        flows_on_match.append([dest_ip, raw_stats.byte_count, raw_stats.duration_sec, raw_stats.duration_nsec])
				#print target
                                #print flows_on_match
                                if flows_on_match != []:
                                        log.debug("Sending to Result Processor...")
                                        #print target
                                        #print flows_on_match

                                        core.MonitoringModuleResultsProcessor.process_flow_rate(target, flows_on_match, tstamp)
                        else:
                                pass #nothing to do

        """
        DEPRECATED
        """
        # # Receive flow stats, extract IPsource, IPdest, PathId.
        # def handle_flow_stats(self, event):
        #         tstamp = get_timestamp()
        #         switch = self.dpid_swId[dpidToStr(event.connection.dpid)]
        #         log.info("Flow stats received from %s", switch)
        #         # Stats parsing
        #         for raw_stat in event.stats:
        #                 #extract flow match fields : Source IP, Dest IP
        #                 source_ip = str(raw_stat.match.nw_src)
        #                 dest_ip =  str(raw_stat.match.nw_dst)
        #                 #find corresponding low_level_target
        #                 for flow_id in core.MonitoringModuleStore.flow_store:
        #                         if source_ip == core.MonitoringModuleStore.flow_store[flow_id][0]:
        #                                 low_level_target = switch + ':' + flow_id
        #                 #extract stats (byte count and duration) of the flow
        #                 new_byte_count = raw_stat.byte_count
        #                 new_duration_sec = raw_stat.duration_sec
        #                 new_duration_nsec = raw_stat.duration_nsec
        #                 log.info("Flow : SrcIP=%s, DstIP=%s, New_Bytes_Count=%d, Duration_sec=%d, Duration_nsec=%d", source_ip, dest_ip, new_byte_count, new_duration_sec, new_duration_nsec)


        """
        DEPRECATED
        """
        # Receive flow stats, extract IPsource, IPdest, PathId.
        #def handle_flow_stats(self, event):
        #        tstamp = get_timestamp()

        #        switch = self.dpid_swId[dpidToStr(event.connection.dpid)]
        #        log.info("Flow stats received from %s", switch)

        #        # Stats parsing
        #        for raw_stat in event.stats:
        #                #extract flow match fields : Source IP, Dest IP
        #                source_ip = str(raw_stat.match.nw_src)
        #                dest_ip =  str(raw_stat.match.nw_dst)
        #                #extract from "output" action (I know, this is tricky) the ToS, then find the corresponding PathID
        #                for action in raw_stat.actions:
        #                        if isinstance(action, of.ofp_action_nw_tos):
        #                                path_id = get_path_id(action.nw_tos)
        #                #extract stats (byte count and duration) of the flow
        #                new_byte_count = raw_stat.byte_count
        #                new_duration_sec = raw_stat.duration_sec
        #                new_duration_nsec = raw_stat.duration_nsec
        #                log.info("Flow : SrcIP=%s, DstIP=%s, Path=%s, New_Bytes_Count=%d, Duration_sec=%d, Duration_nsec=%d", source_ip, dest_ip, path_id, new_byte_count, new_duration_sec, new_duration_nsec)



def get_path_id(tos):
        for path_id, path_entry in core.MonitoringModuleStore.path_store.items():
                if path_entry[3] == tos:
                        return path_id


def get_timestamp():
        dt = datetime.now()
        return dt

def launch (manager = DEFAULT_MGR):
	   core.registerNew(MonitoringModuleResultsInterpreter, int(manager))
