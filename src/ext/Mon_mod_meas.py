from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.addresses import IPAddr
from pox.lib.util import str_to_dpid
from pox.lib.util import dpid_to_str
from pox.lib.recoco import Timer
import const
import sys
from datetime import datetime
DEFAULT_MGR = 0

log = core.getLogger()

class MonitoringModuleMeasurements(object):

        def __init__(self, manager):
                self.mgr = manager
                self.swId_dpid = {} # Map <switch_id, datapath_id>
                self.init_swId_dpid()
                #Test(s):
                #Timer(5, self.switch_flow_poll, args = ["s6", ["P2", "P1"]], recurring=True)
                #Timer(5, self.switch_port_poll, args = ["s8", ["eth1", "eth2", "eth3"]], recurring=True)
        def init_swId_dpid(self):
                log.debug("Loading switch_id/dpid for local cluster")
                for sw in core.MonitoringModuleStore.sw_cluster:
                        sw_dpid = int(sw[1:])
                        self.swId_dpid[sw] = dpid_to_str(sw_dpid)
                print self.swId_dpid

        #Poll a single switch for port stats
        def switch_port_poll(self, switch, port_list):
                log.debug("Polling %s for port stats", switch)
                dpid = str_to_dpid(self.swId_dpid[switch]) #openflow datapath (switch )identifier
                if len(port_list) == len(core.MonitoringModuleStore.sw_port_store[switch]):
                        #send a single of port stats request, for all ports
                        core.openflow.connections[dpid].send(of.ofp_stats_request(body=of.ofp_port_stats_request()))
                else:
                        #send separate (per port) message
                        #TODO One goo alternative is to require aggrgate flow stats with out_port set.
                        for p in port_list:
                                port_number = int(p[3:]) #strip "eth" from port id
                                msg_body = of.ofp_port_stats_request()
                                msg_body.port_no = port_number
                                msg = of.ofp_stats_request()
                                msg.body = msg_body
				#print '\nSize message: ' + str(sys.getsizeof(msg))
                                try:
					core.openflow.connections[dpid].send(msg)
				except Exception, e:
					print '@@@@@@@@@@@@@@@@@@@@@@@@@@',e

        def switch_flow_poll(self, switch, flow_match_list):
                log.debug("Polling %s for flow stats", switch)
                dpid = str_to_dpid(self.swId_dpid[switch]) #openflow datapath (switch )identifier
                for f in flow_match_list:
                        #the match will be on the server_ip and on the output port (which is derived from the path)
                        server_ip = f.split(':')[0]
                        path_tos = f.split(':')[1]
                        for path in core.MonitoringModuleStore.path_store:
                                if core.MonitoringModuleStore.path_store[path][3] == int(path_tos):
                                        #find first link on path and corresponding port on edge switch
					if const.REVERSE_FLOWS == False:
                                        	first_path_link = core.MonitoringModuleStore.path_store[path][2][0]
                                        	path_out_port = int( (core.MonitoringModuleStore.topo_store[first_path_link][1])[3:] )
                                        	#create stats request msg and call the controller
                                        	msg_body = of.ofp_flow_stats_request()
                                        	msg_body.out_port = path_out_port
                                        	msg_body.match.dl_type = 0x800
                                        	msg_body.match.nw_dst = server_ip
                                        	msg = of.ofp_stats_request()
					elif const.REVERSE_FLOWS == True:
						first_path_link = core.MonitoringModuleStore.path_store[path][2][0]
                                                path_out_port = int( (core.MonitoringModuleStore.topo_store[first_path_link][1])[3:] )
                                                #create stats request msg and call the controller
                                                msg_body = of.ofp_flow_stats_request()
                                                msg_body.match.dl_type = 0x800
                                                msg_body.match.nw_src = server_ip
                                                msg = of.ofp_stats_request()					
                                        msg.body = msg_body
					#print msg
					try:
                                        	core.openflow.connections[dpid].send(msg)
					except:
						pass


        """
        DEPRECATED
        """
        # #Poll a single switch for individual flow stats (1 flow = 1 source client).
        # #The match condition is on Source IP address
        # def switch_flow_poll(self, switch, flow_list):
        #         log.debug("Polling %s for flow stats", switch)
        #         dpid = str_to_dpid(self.swId_dpid[switch]) #openflow datapath (switch )identifier
        #         print flow_list
        #         for f in flow_list:
        #                 client_ip = IPAddr(core.MonitoringModuleStore.flow_store[f][0])
        #                 server_ip = IPAddr(cost.SERVER)
        #                 msg_body = of.ofp_flow_stats_request()
        #                 msg_body.match.dl_type = 0x800
        #                 msg_body.match.nw_src = client_ip
        #                 msg = of.ofp_stats_request()
        #                 msg.body = msg_body
        #                 core.openflow.connections[dpid].send(msg)



        """
        DEPRECATED
        """
        #Poll a single switch for individual flow stats (1 flow = 1 source client).
        #The match condition is on the Path Id. (Type of Service)
        #def switch_flow_poll(self, switch, path_id_list):
        #        log.debug("Polling %s for flow stats", switch)
        #        dpid = str_to_dpid(self.swId_dpid[switch])
        #        for path_id in path_id_list:

        #                #find first link on path and corresponding port on edge switch
        #                first_path_link = core.MonitoringModuleStore.path_store[path_id][2][0]
        #                path_out_port = int( (core.MonitoringModuleStore.topo_store[first_path_link][1])[3:] )

        #                #create stats request
        #                msg_body = of.ofp_flow_stats_request()
        #                msg_body.out_port = path_out_port
        #                msg = of.ofp_stats_request()
        #                msg.body = msg_body
        #                core.openflow.connections[dpid].send(msg)



def launch (manager = DEFAULT_MGR):
        core.registerNew(MonitoringModuleMeasurements, int(manager))
