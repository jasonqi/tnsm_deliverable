# Scheduler:
#   the scheduler receives from the requirement processor the following info:
#       1) List of the low level targets to be SCHEDULED
#       2) List of the low level targets to be RESCHEDULED ()


# Example of schedule_table entry
#   self.schedule_table[1] = ["PORT-POLL", "s1" ["eth1", "eth2"], 10, datetime.now().time(), Timer(10, self.handle_new_event, args = [1], recurring=True)]
# [ops. type, resource(switch,..), low_level_targets, period, time_of_insert, timer_based_thread]

from pox.core import core
from pox.lib.recoco import Timer
from datetime import datetime
from time import sleep
import const

DEFAULT_MGR = 0

log = core.getLogger()

class MonitoringModuleScheduler(object):
        def __init__(self, manager):
                self.mgr = manager
                self.schedule_table = {} #list of scheduled measurement ops. Key : [switch, [list of ports/flows], init_time, timer_object]
		self.schedule_table_new = {}

        #Updating schedule_table...

        def schedule(self, to_schedule_list, to_reschedule_list, ops_type):
                #select scheduling function according to the type of ops.
                if ops_type == "PORT:RATE":
                        self.schedule_port_rate(to_schedule_list, to_reschedule_list)
                elif ops_type == "FLOW:RATE":
                        self.schedule_flow_rate(to_schedule_list, to_reschedule_list)
                else:
                        log.debug("Only PORT:RATE and FLOW:RATE operations are supported now")


	def schedule_port_rate_new (self, new_period, low_level_target):
		for entry in self.schedule_table:
			#print "Entry = ", entry
			temp = str(self.schedule_table[entry][2])
			temp = temp[2:-2]
			this_target = str(self.schedule_table[entry][1])+":"+str(temp)
			#print "expected target = ", this_target
			if this_target == low_level_target:
				self.schedule_table[entry][5] = Timer(new_period, self.handle_port_poll, args=[entry], recurring=False)
				self.schedule_table[entry][3] = new_period
				#print "scheduled for ",self.schedule_table[entry]
		#print "&&&&&&&&&&"
		#print "schedule_table = ", self.schedule_table
		#print "&&&&&&&&&&"



        def schedule_port_rate(self, to_schedule_list, to_reschedule_list):
                #low level targets are received in the the format switch_id:port_id
                for target in to_schedule_list:
                        switch = target.split(':')[0]
                        port = target.split(':')[1]
                        #retreive required frequency from low_level_targets table
                        period = core.MonitoringModuleRequirementInterpreter.low_level_targets[target][2]
                        #check if the main resource (e.g. the switch) is already in the table, for port-poll ops.
                        similar_entry_found_flag = False
                        for index in self.schedule_table:
                                if self.schedule_table[index][1] == switch and self.schedule_table[index][0] == "PORT:RATE":
                                        similar_entry_found_flag = True
                                        existing_period = self.schedule_table[index][3]
                                        if period == existing_period:
                                                self.schedule_table[index][2].append(port)
                                        elif period < existing_period and existing_period % period == 0:
                                                #the required frequency is multiple of the already existing one. Aligning periods...
                                                now = datetime.now()
                                                existing_start_time = self.schedule_table[index][4]
                                                delta = (now - existing_start_time).seconds + ((now - existing_start_time).microseconds/1000000.0)
                                                aligning_time = period - (delta % existing_period)
                                                #partially merge the low level targets in case new_freq = 2*old_freq
                                                if existing_period == 2 * period:
                                                        self.schedule_table[index][2].append(port)
                                                        Timer(aligning_time, self.align_events, args = [["PORT:RATE", switch, [port], 2 * period]])
                                                else:
                                                        #no merging is done
                                                        Timer(aligning_time, self.align_events, args = [["PORT:RATE", switch, [port], period]])
                                        else:
                                                #add new entry. TODO Check how this can be optimized
                                                new_id = len(self.schedule_table)
                                                self.schedule_table[new_id] = ["PORT:RATE", switch, [port], period, datetime.now(), Timer(period, self.handle_port_poll, args=[new_id], recurring=False)]
						#address the bug for unknown reason by duplicating
						self.schedule_table[new_id] = ["PORT:RATE", switch, [port], period, datetime.now(), Timer(period, self.handle_port_poll, args=[new_id], recurring=False)]

                                        break
                        if similar_entry_found_flag == False:
                                #add new entry.
                                new_id = len(self.schedule_table)
                                self.schedule_table[new_id] = ["PORT:RATE", switch, [port], period, datetime.now(), Timer(period, self.handle_port_poll, args=[new_id], recurring=False )]
				#address the bug for unknown reason by duplicating
				self.schedule_table[new_id] = ["PORT:RATE", switch, [port], period, datetime.now(), Timer(period, self.handle_port_poll, args=[new_id], recurring=False )]
			print "==============================="
			print "Initial Schedule Table : ", self.schedule_table	
			print "==============================="
                        core.MonitoringModuleRequirementInterpreter.low_level_targets[target][4] == "SCHEDULED"
                for target in to_reschedule_list:
                        pass
                        #TODO Handle this case!




        def schedule_flow_rate(self, to_schedule_list, to_reschedule_list):
                #low level targets are received in the the format switch_id:flow_id
                for target in to_schedule_list:
                        edge_switch = target.split(':')[0]
                        server = target.split(':')[1]
                        path_code = target.split(':')[2] #path code on the datapath (tos value)
                        #retreive required frequency from low_level_targets table
                        period = core.MonitoringModuleRequirementInterpreter.low_level_targets[target][2]
                        #check if the main resource (e.g. the switch) is already in the table, for flow-poll ops.
                        similar_entry_found_flag = False
                        for index in self.schedule_table:
                                if self.schedule_table[index][1] == edge_switch and self.schedule_table[index][0] == "FLOW:RATE":
                                        similar_entry_found_flag = True
                                        existing_period = self.schedule_table[index][3]
                                        if period == existing_period:
                                                self.schedule_table[index][2].append(server + ':' + path_code)
                                        elif period < existing_period and existing_period % period == 0:
                                                #the required frequency is multiple of the already existing one. Aligning periods...
                                                now = datetime.now()
                                                existing_start_time = self.schedule_table[index][4]
                                                delta = (now - existing_start_time).seconds + ((now - existing_start_time).microseconds/1000000.0)
                                                aligning_time = period - (delta % existing_period)
                                                #partially merge the low level targets in case new_freq = 2*old_freq
                                                if existing_period == 2 * period:
                                                        self.schedule_table[index][2].append(flow)
                                                        Timer(aligning_time, self.align_events, args = [["FLOW:RATE", edge_switch, [server + ':' + path_code], 2 * period]])
                                                else:
                                                        #no merging is done
                                                        Timer(aligning_time, self.align_events, args = [["FLOW:RATE", edge_switch, [server + ':' + path_code], period]])
                                        else:
                                                #add new entry.
                                                new_id = len(self.schedule_table)
                                                self.schedule_table[new_id] = ["FLOW:RATE", edge_switch, [server + ':' + path_code], period, datetime.now(), Timer(period, self.handle_flow_poll, args=[new_id], recurring=True)]
                                        break
                                        #TODO Check how this can be optimized
                        if similar_entry_found_flag == False:
                                #add new entry.
                                new_id = len(self.schedule_table)
                                self.schedule_table[new_id] = ["FLOW:RATE", edge_switch, [server + ':' + path_code], period, datetime.now(), Timer(period, self.handle_flow_poll, args=[new_id], recurring=True)]
                        core.MonitoringModuleRequirementInterpreter.low_level_targets[target][4] == "SCHEDULED"
                print self.schedule_table
                for target in to_reschedule_list:
                        pass
                        #TODO Handle this case!




        """
        DEPRECATED
        """
        # def schedule_flow_rate(self, to_schedule_list, to_reschedule_list):
        #         #low level targets are received in the the format switch_id:flow_id
        #         for target in to_schedule_list:
        #                 switch = target.split(':')[0]
        #                 flow = target.split(':')[1]
        #                 #retreive required frequency from low_level_targets table
        #                 period = core.MonitoringModuleRequirementInterpreter.low_level_targets[target][2]
        #                 #check if the main resource (e.g. the switch) is already in the table, for flow-poll ops.
        #                 similar_entry_found_flag = False
        #                 for index in self.schedule_table:
        #                         if self.schedule_table[index][1] == switch and self.schedule_table[index][0] == "FLOW:RATE":
        #                                 similar_entry_found_flag = True
        #                                 existing_period = self.schedule_table[index][3]
        #                                 if period == existing_period:
        #                                         self.schedule_table[index][2].append(flow)
        #                                 elif period < existing_period and existing_period % period == 0:
        #                                         #the required frequency is multiple of the already existing one. Aligning periods...
        #                                         now = datetime.now()
        #                                         existing_start_time = self.schedule_table[index][4]
        #                                         delta = (now - existing_start_time).seconds + ((now - existing_start_time).microseconds/1000000.0)
        #                                         aligning_time = period - (delta % existing_period)
        #                                         #partially merge the low level targets in case new_freq = 2*old_freq
        #                                         if existing_period == 2 * period:
        #                                                 self.schedule_table[index][2].append(flow)
        #                                                 Timer(aligning_time, self.align_events, args = [["FLOW:RATE", switch, [flow], 2 * period]])
        #                                         else:
        #                                                 #no merging is done
        #                                                 Timer(aligning_time, self.align_events, args = [["FLOW:RATE", switch, [flow], period]])
        #                                 else:
        #                                         #add new entry.
        #                                         new_id = len(self.schedule_table)
        #                                         self.schedule_table[new_id] = ["FLOW:RATE", switch, [flow], period, datetime.now(), Timer(period, self.handle_flow_poll, args=[new_id], recurring=True)]
        #                                 break
        #                                 #TODO Check how this can be optimized
        #                 if similar_entry_found_flag == False:
        #                         #add new entry.
        #                         new_id = len(self.schedule_table)
        #                         self.schedule_table[new_id] = ["FLOW:RATE", switch, [flow], period, datetime.now(), Timer(period, self.handle_flow_poll, args=[new_id], recurring=True)]
        #                 core.MonitoringModuleRequirementInterpreter.low_level_targets[target][4] == "SCHEDULED"
        #         for target in to_reschedule_list:
        #                 pass
        #                 #TODO Handle this case!



        def align_events(self, new_entry):
                if new_entry[0] == "PORT:RATE":
                        #add new entry.
                        log.debug("Aligning port:rate events")
                        new_id = len(self.schedule_table)
                        self.schedule_table[new_id] = [new_entry[0], new_entry[1], new_entry[2], new_entry[3], datetime.now(), Timer(new_entry[3], self.handle_port_poll, args=[new_id], recurring= True)]
                elif new_entry[0] == "FLOW:RATE":
                        #add new entry.
                        log.debug("Aligning flow:rate events")
                        new_id = len(self.schedule_table)
                        self.schedule_table[new_id] = [new_entry[0], new_entry[1], new_entry[2], new_entry[3], datetime.now(), Timer(new_entry[3], self.handle_flow_poll, args=[new_id], recurring= True)]
                else:
                        log.debug("Only PORT:RATE and FLOW:RATE operations are supported now")




        #Handlers for new measurement operations (on timer expirations)

        def handle_port_poll(self, index):
                #print "Port:rate event at time " + str(datetime.now().time()) + " : " + self.schedule_table[index][1]
                f_log = open('lat.log', 'a')
		#log
		sw = self.schedule_table[index][1]
		for port in self.schedule_table[index][2]:
			f_log.write(core.MonitoringModuleRequirementInterpreter.low_level_targets[sw+':'+port][1][0] + " SCHED " + str(datetime.now().time()) + ' ' + str(const.PORT_POLL_BYTES) + ' ' + str(core.MonitoringModuleStore.mgr_lat[self.schedule_table[index][1]]*2) + '\n')
		f_log.close()
		#print self.schedule_table[index][2]
                #self.schedule_table[index][5].cancel() #Test: cancelling timer-based thread

                #call to the Measurements submodule
		#core.MonitoringModuleMeasurements.switch_port_poll(self.schedule_table[index][1], self.schedule_table[index][2])

		"""
		TODO test mgr-sw latencies
		"""
		Timer(core.MonitoringModuleStore.mgr_lat[self.schedule_table[index][1]] * 0.002, core.MonitoringModuleMeasurements.switch_port_poll, args = [self.schedule_table[index][1], self.schedule_table[index][2]]) #latency in secs * 2 (round trip)

        #Handlers for new measurement operations (on timer expirations)

        def handle_flow_poll(self, index):
                #print "Flow:rate event at time " + str(datetime.now().time()) + " : " + self.schedule_table[index][1]
                #print self.schedule_table[index][2]

                #self.schedule_table[index][5].cancel() #Test: cancelling timer-based thread

                #call to the Measurements submodule
                core.MonitoringModuleMeasurements.switch_flow_poll(self.schedule_table[index][1], self.schedule_table[index][2])

def launch (manager = DEFAULT_MGR):
	   core.registerNew(MonitoringModuleScheduler, int(manager))
