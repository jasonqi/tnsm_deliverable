#This class is a container for methods which 1) process and 2) associate the low-level results
#to high level objectives.

#OBS.

from pox.core import core
from datetime import datetime
import math

DEFAULT_MGR = 0

log = core.getLogger()

class MonitoringModuleResultsProcessor(object):
        def __init__(self, manager):
                self.mgr = manager
                log.debug("Initializing Monitoring Module Result Processor ...")
                self.local_cong_det_app = "CD_" + str(self.mgr)
                self.local_path_sel_app = "PS_" + str(self.mgr)
                self.local_server_sel_app = "SS_" + str(self.mgr)
		#TODO Add support for future applications here (or move it to config file)
                self.port_rate_table = {}
                self.flow_rate_table = {}
		self.prev_tx_rate = 0
		self.prev_rx_rate = 0 
		self.result_history = []
		self.time_history = []
		self.R = 0

        # adptive scheme 1: check percentage increment
        def adaptive_1 (self, limit_high, limit_low, parameter_mul, parameter_div, rx_rate, tx_rate, low_level_target):
	    print "%%%%%%%%%%%%%%%%"
	    print "mgr_id = ", self.mgr
	    print "%%%%%%%%%%%%%%%%"
            if self.mgr != 1:
		core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2], low_level_target)
		return
            if str(low_level_target) != 's1:eth3':
                #core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2], low_level_target)
		return
		
	    this_period = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2] 
            #if abs(tx_rate-self.prev_tx_rate) > self.prev_tx_rate/10.0 or abs(rx_rate-self.prev_rx_rate) > self.prev_rx_rate/10.0:
            if abs(tx_rate-self.prev_tx_rate) > self.prev_tx_rate/10.0:
                this_period /= parameter_div
                if this_period <= limit_low:
                    this_period = limit_low
                print 'Frequency_UP_', this_period
                #print core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]

            else:
                this_period *= parameter_mul
                if this_period >= limit_high:
                        this_period= limit_high
                print 'Frequency_DOWN_', this_period
                #%print core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]

            #core.MonitoringModuleScheduler.schedule([str(low_level_target)], [], "PORT:RATE")
            #core.MonitoringModuleScheduler.schedule_port_rate([str(low_level_target)], [])
            self.prev_tx_rate = tx_rate
            self.prev_rx_rate = rx_rate
            #print core.MonitoringModuleScheduler.schedule_table_new
	    #core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2])
	    #core.MonitoringModuleScheduler.schedule_table_new[low_level_target]= core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]
	    
	    for this_target in core.MonitoringModuleRequirementInterpreter.low_level_targets:
		core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2] = this_period
            	core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2], this_target)
            	#core.MonitoringModuleScheduler.schedule_table_new[low_level_target]= core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2]
            

  
        # adptive scheme 3
	def adaptive_3 (self, limit_high, limit_low, rx_rate, tx_rate, low_level_target, delta_time):
	    
	    if str(low_level_target) != 's1:eth3':
		return

	    this_period = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]
            f_log = open('adaptive3.log', 'a')
            if len(self.result_history) < 4:
                # Compute prediction rate
                print 'List Not Full'
                self.result_history.append(tx_rate)
                if len(self.time_history) == 0:
                    self.time_history.append(0)
                else:
                    self.time_history.append(delta_time+self.time_history[-1])
                print 'No Change Frequency', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]

            else:
                # Compute linear prediction
                prediction = 0
                for i in range(len(self.result_history)-1):
                    prediction += (self.result_history[i+1]-self.result_history[i])/(self.time_history[i+1]-self.time_history[i])
                prediction = prediction*(self.time_history[-1]-self.time_history[-2])/(len(self.result_history)-1)+self.result_history[-1]
                 
                # Compute quality of prediction, adapt monitoring period
		flag = None
                if prediction == 0 or round(prediction*8 / 1000000.0, 2) == 0:
                    temp = 0
                    this_period = 1
                    flag = 1
                elif flag == None and abs(round(prediction*8 / 1000000.0, 2) - round(tx_rate*8 / 1000000.0, 2))/round(prediction*8 / 1000000.0, 2) >= 0.9:
                    this_period = limit_low
                    flag = 2
                elif flag == None and abs(round(prediction*8 / 1000000.0, 2) - round(tx_rate*8 / 1000000.0, 2))/round(prediction*8 / 1000000.0, 2) <= 0.1:
                    this_period = limit_high
                    flag = 3
                elif  flag == None:
                    temp = abs(round(prediction*8 / 1000000.0, 2) - round(tx_rate*8 / 1000000.0, 2))/round(prediction*8 / 1000000.0, 2)
                    this_period = limit_high * temp
                    flag = 4
                    if prediction == 0:
                        temp = 0
                        this_period = 1
                
                # Update sample queue
                x0 = self.result_history.pop(0)
                self.result_history.append(tx_rate)
                x1 = self.time_history.pop(0)
                self.time_history.append(delta_time+self.time_history[-1])
                    
                # Log results
                print 'Actual rate = ', tx_rate*8 / 1000000.0
                f_log.write('Actual rate = '+str(tx_rate*8 / 1000000.0)+'\n')
                print 'Previous rate = ', self.result_history[-2]*8 / 1000000.0
                f_log.write('Previous rate = '+str(self.result_history[-2]*8 / 1000000.0)+'\n')
                if flag == 1:
                    print('Frequency_Exception_'+str(this_period))
                    f_log.write('Frequency_Exception_'+str(this_period)+'\n')
                elif flag == 2:
                    print('Frequency_Maximum_'+str(this_period))
                    f_log.write('Frequency_Maximum_'+str(this_period)+'\n')
                elif flag == 3:
                    print('Frequency_Minimum_'+str(this_period))
                    f_log.write('FFrequency_Minimum_'+str(this_period)+'\n')
                elif flag == 4:
                    print('Frequency_Linear_Mapped_'+str(this_period))
                    f_log.write('Frequency_Linear_Mapped_'+str(this_period)+'\n')
                print('\n--------------------------------\n\n')
                f_log.write('\n--------------------------------\n\n')
                print 'Prediction = ', prediction *8 / 1000000.0
                f_log.write('Prediction = '+str(prediction *8 / 1000000.0)+'\n')
                f_log.close()
		
	    '''
	    # Pass new monitoring frequency accordingly
            core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2])
            core.MonitoringModuleScheduler.schedule_table_new[low_level_target]= core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]                	      '''
	    for this_target in core.MonitoringModuleRequirementInterpreter.low_level_targets:
                core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2] = this_period
                core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2], this_target)
                #core.MonitoringModuleScheduler.schedule_table_new[low_level_target]= core.MonitoringModuleRequirementInterpreter.low_level_targets[this_target][2] 
                
        # adptive scheme 2: check percentage increment
        def adaptive_2 (self, limit_high, limit_low, parameter_mul, parameter_div, rx_rate, tx_rate, low_level_target, delta_time, history_length=23):

		f_log = open('adaptive2.log', 'a')
		# Compute prediction rate
		if len(self.result_history) < history_length:
			# Update history results: If queue is not full, append new mearsurement
			if len(self.result_history)< history_length:
				print "List Not Full"
				self.result_history.append(tx_rate)
				if len(self.time_history) == 0:
					self.time_history.append(0)
				else:
					self.time_history.append(delta_time+self.time_history[-1])
				print 'No Change Frequency', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]

			# If the queue is full, pop the oldest data and append the new one
			else:
				print "List Full"
				x0 = self.result_history.pop(0)		
				self.result_history.append(tx_rate)
				x1 = self.time_history.pop(0)
				self.time_history.append(delta_time+self.time_history[-1])
				print 'No Change Frequency', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]


		else:
			# Compute prediction using EWMA
			prediction = 0
			for i in range(len(self.result_history)-1):
				prediction += (self.result_history[i+1]-self.result_history[i])/(self.time_history[i+1]-self.time_history[i])
			prediction = prediction*(self.time_history[-1]-self.time_history[-2])/(len(self.result_history)-1)+self.result_history[-1]


			if round(prediction*8 / 1000000.0, 2)- round(self.result_history[-1]*8 / 1000000.0, 2) == 0:
					numerator=0.01
			else:
				numerator=round(prediction*8 / 1000000.0, 2)- round(self.result_history[-1]*8 / 1000000.0, 2)

			if round(tx_rate*8 / 1000000.0, 2)-round(self.result_history[-1]*8 / 1000000.0, 2) == 0:
				denominator=0.01
			else:
				denominator=round(tx_rate*8 / 1000000.0, 2)-round(self.result_history[-1]*8 / 1000000.0, 2)

			self.R = abs(numerator/denominator)

			if abs(round(prediction*8 / 1000000.0, 2) - round(tx_rate*8 / 1000000.0, 2)) <= 0.1:
				self.R = 1

			# Compute acceptable range
			Mean = sum(self.result_history)/len(self.result_history)

			std = 0
			for i in self.result_history:
				std += (i-Mean)*(i-Mean)
			std = math.sqrt(std/len(self.result_history))
			
			try:
                                Upper = 1.25
                                Lower = 0.75
				#Upper = abs((round(Mean + 1.5*std, 2) - round(self.result_history[-1],2))/(round(tx_rate, 2) - round(self.result_history[-1],2)))
				#Lower = abs((round(Mean - 1.5*std, 2) - round(self.result_history[-1],2))/(round(tx_rate, 2) - round(self.result_history[-1],2)))
			except:
				Upper = 1.01
				Lower = 0.99

			flag = 0
			# Compare results
			#if self.R>=Upper or self.R<=Lower:
			if self.R>=Upper:
				# Hit: Reduce monitoring frequency
				core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2] *= parameter_mul
				if core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]>= limit_high:
					core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]= limit_high
				#print 'Frequency_DOWN_', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]
				flag = 1


			elif self.R<=Lower:
				# Miss: Increase monitoring frequency
				core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2] /= parameter_div
				if core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]<= limit_low:
					core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]= limit_low
				#print 'Frequency_UP_', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]
				flag = 2

			else:
				#print 'Frequency_Unchanged_', core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]
				flag = 3
			# Update history results: If queue is not full, append new mearsurement
			if len(self.result_history)< history_length:
				self.result_history.append(tx_rate)
				self.time_history.append(delta_time+self.time_history[-1])

			# If the queue is full, pop the oldest data and append the new one
			else:
				x0 = self.result_history.pop(0)		
				self.result_history.append(tx_rate)
				x1 = self.time_history.pop(0)
				self.time_history.append(delta_time+self.time_history[-1])

			f_log.write('Actual rate= '+str(tx_rate*8 / 1000000.0)+'\n')
			f_log.write('Previous rate= '+str(self.result_history[-2]*8 / 1000000.0)+'\n')
			f_log.write('R = '+str(self.R)+'\n')
			f_log.write('Mean = '+str(Mean* 8 / 1000000.0)+'\n')
			f_log.write('std = '+str(std* 8 / 1000000.0)+'\n')
			f_log.write('Upper = '+str(Upper)+'\n')
			f_log.write('Lower = '+str(Lower)+'\n')
			if flag == 1:
				f_log.write('Frequency_DOWN_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			elif flag == 2:
				f_log.write('Frequency_UP_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			elif flag == 3:
				f_log.write('Frequency_Unchanged_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			f_log.write('\n--------------------------------\n\n')
			f_log.write('Prediction = '+str(prediction *8 / 1000000.0)+'\n')
			f_log.close()
			
			print 'Actual rate= ', tx_rate*8 / 1000000.0
			print 'Previous rate= ', self.result_history[-2]*8 / 1000000.0
			print 'R = ', self.R
			print 'Mean = ', Mean* 8 / 1000000.0
			print 'std = ', std* 8 / 1000000.0
			print 'Upper = ', Upper
			print 'Lower = ', Lower
			if flag == 1:
				print('Frequency_DOWN_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			elif flag == 2:
				print('Frequency_UP_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			elif flag == 3:
				print('Frequency_Unchanged_'+str(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]))
			print('\n--------------------------------\n\n')
			print 'Prediction = ', prediction *8 / 1000000.0

			


		# Pass new monitoring frequency accordingly
		core.MonitoringModuleScheduler.schedule_port_rate_new(core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2])
		core.MonitoringModuleScheduler.schedule_table_new[low_level_target]= core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][2]

        def process_port_rate(self, low_level_target, tx_bytes, rx_bytes, tstamp):
                #update port_rate_table
                if low_level_target not in self.port_rate_table:
                        #add new entry, then return (no previous measurement)
                        self.port_rate_table[low_level_target] = [tx_bytes, rx_bytes, tstamp]
                        return
                else:
                        #collect previous tx_bytes, rx_bytes, tstamp
                        prev_tx_bytes = self.port_rate_table[low_level_target][0]
                        prev_rx_bytes = self.port_rate_table[low_level_target][1]
                        prev_tstamp = self.port_rate_table[low_level_target][2]
                        #compute byte count (and time) increments
                        delta_tx = tx_bytes - prev_tx_bytes
                        delta_rx = rx_bytes - prev_rx_bytes
                        delta_time = (tstamp - prev_tstamp).seconds + (tstamp - prev_tstamp).microseconds/1000000.0
                        #compute port rates (bytes/sec)
                        tx_rate = delta_tx / delta_time
                        rx_rate = delta_rx / delta_time
                        #replace port_rate_table entry
                        self.port_rate_table[low_level_target] = [tx_bytes, rx_bytes, tstamp]
                        #associate to high level target
                        self.associate_port_rate(low_level_target, tx_rate, rx_rate, tstamp)

			#invoke adaptive monitoring algorithm (upperlimit/s, lowerlimit/s, multiplication_constant, division_constant, rx_rate, tx_rate, low_level_target)
			self.adaptive_3(5, 0.1, rx_rate, tx_rate, low_level_target, delta_time)
			#self.adaptive_1(3, 0.1, 2, 8, rx_rate, tx_rate, low_level_target)

        def associate_port_rate(self, low_level_target, tx_rate, rx_rate, tstamp):
                #associate to high level target(s)
                high_level_target_list = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][1]
                requirement_id_list = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][3]
                for requirement_id in requirement_id_list:
                        #fetch the high level requiment from the app_requirements table
                        requirement = core.MonitoringModuleRequirementInterpreter.app_requirements[requirement_id]
                        for high_level_target in high_level_target_list:
                                if high_level_target in requirement[2]:
                                        if requirement[1] == ['LINK', 'UTIL', '']:
                                                if requirement[0]== self.local_cong_det_app:
                                                        #send to local Congestion Detection App.
                                                        msg = []
                                                        msg.append([])
                                                        msg[0].append(high_level_target)
                                                        msg[0].append(tx_rate)
                                                        msg[0].append(rx_rate)
                                                        msg[0].append(tstamp)
                                                        core.AppCongestionDetection.handle_link_util_updates(msg)
                                                elif requirement[0]== self.local_server_sel_app:
                                                        #send to local Server Selection App.
                                                        msg = []
                                                        msg.append([])
                                                        msg[0].append(high_level_target)
                                                        msg[0].append(tx_rate)
                                                        msg[0].append(rx_rate)
                                                        msg[0].append(tstamp)
                                                        core.AppServerSelection.handle_link_util_updates(msg)
                                                else:
                                                        log.debug("Only local Congestion Detection or Server Selection App. can register this type of target")
                                        else:
                                                log.debug("Only LINK:UTIL high-level targets supported for PORT:RATE measurements")
                                                #TODO consider other cases (if any...)



        def process_flow_rate(self, low_level_target, flow_list, tstamp):
                list_processed_flows = []
                for f in flow_list:
                        flow_id = low_level_target + ':' + f[0] #append src_ip to low_level_target identifier
                        #update flow_rate_table
                        if flow_id not in self.flow_rate_table:
                                #the flow is new: store counter, compute rate based on the duration_sec, duration_nsec, and store it
                                last_byte_count = f[1]
                                flow_duration = (f[2] + f[3]/1000000000.0)
                                flow_rate = last_byte_count / flow_duration
                                self.flow_rate_table[flow_id] = [last_byte_count, flow_duration, flow_rate, tstamp]
                        else:
                                #the flow is not new, update existing entry
                                last_byte_count = f[1]
                                flow_duration = (f[2] + f[3]/1000000000.0)
                                prev_byte_count = self.flow_rate_table[flow_id][0]
                                prev_flow_duration = self.flow_rate_table[flow_id][1]
                                if flow_duration==prev_flow_duration or flow_duration-prev_flow_duration<0.5 or last_byte_count<prev_byte_count:
					"""
					print 'Time : ' + str(datetime.now())
					print 'GHOST FLOW'
					print prev_flow_duration
					print flow_duration
                                        #the flow was associated with this path at some point in the past (not in the last period)
                                        flow_rate = last_byte_count / flow_duration
					"""
					continue #FIXME Solve bugs when >1 stats replies are received
                                else:
                                        flow_rate = (last_byte_count - prev_byte_count) / (flow_duration - prev_flow_duration)
					"""
					print 'Time : ' + str(datetime.now())
					print last_byte_count
                                        print prev_byte_count
                                        print flow_duration
					print prev_flow_duration
					"""
                                self.flow_rate_table[flow_id] = [last_byte_count, flow_duration, flow_rate, tstamp]
                        list_processed_flows.append(flow_id)
                #associate to high level target
                if list_processed_flows != []:
			self.associate_flow_rate(low_level_target, list_processed_flows)



        def associate_flow_rate(self, low_level_target, list_processed_flows):
                #associate to high level target(s)
                high_level_target_list = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][1]
                requirement_id_list = core.MonitoringModuleRequirementInterpreter.low_level_targets[low_level_target][3]
                for requirement_id in requirement_id_list:
                        #fetch the high level requiment from the app_requirements table
                        requirement = core.MonitoringModuleRequirementInterpreter.app_requirements[requirement_id]
                        for high_level_target in high_level_target_list:
                                if high_level_target in requirement[2]:
                                        if requirement[1] == ["PATH", "UTIL", "flow"]:
                                                #associate and send msg to App.
                                                if requirement[0]== self.local_path_sel_app:
                                                        msg = []
                                                        for flow in list_processed_flows:
                                                                msg_item = []
                                                                msg_item.append(high_level_target)
                                                                msg_item.append(flow.split(':')[3]) #client_ip(or prefix))
                                                                msg_item.append(self.flow_rate_table[flow][2]) #rate
                                                                msg_item.append(self.flow_rate_table[flow][3]) #meas. tstamp
                                                                msg.append(msg_item)
                                                        log.info("Sending update message to local App. Path Selection")
                                                        core.AppPathSelection.handle_path_util_updates(msg)
                                                else:
                                                        log.debug("Only local Path Selection App. can register this type of target")
                                        else:
                                                log.debug("Only PATH:UTIL high-level targets supported for FLOW:RATE measurements")



def launch (manager = DEFAULT_MGR):
	   core.registerNew(MonitoringModuleResultsProcessor, int(manager))
