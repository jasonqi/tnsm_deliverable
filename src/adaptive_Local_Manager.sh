#!/bin/bash

#Run the Pox Controller core and the Local Manager modules
let "port_no = 6633 + $1"
echo $port_no
if [ $1 -gt 0 ]
        then
        ./pox.py openflow.of_01 -port=$port_no Path_inst -manager=$1 Mon_mod_store -manager=$1 Mon_mod_req_interpreter -manager=$1 Mon_mod_sched_adaptive -manager=$1 Mon_mod_meas -manager=$1 Mon_mod_res_interpreter -manager=$1 Mon_mod_res_processor_adaptive -manager=$1 App_cong_det -manager=$1 App_path_selection -manager=$1 Udp_if -manager=$1
else
        echo "Invalid Local Manager Id"
fi
