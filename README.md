Experiment repository
********************************************************************************

CONTENTS:

  ./src : Local manager implementation with/without adaptive nature
 
  ./mininet : Network emulation environment (copy to Mininet) for different LM setups. This contains the programs executed at clients and servers

  ./lm_config : Local manager configuration directories

********************************************************************************

INSTRUCTIONS (Ex. Set up Mininet environment with 2 Local managers):

1) To run cloud gaming experiments, execute 'automation_test.py' in a specific setup under the mininet directory.

2) For each setup, modify Test_input/clientX.sh with './ultra_ping/echo.py --client 10.0.0.11 --output_filename 1_client_lat.log --output_time 1_client_time.log' for cloud gaming traffic.

3) For each setup, modify Test_input/clientX.sh with 'python run_request_trace.py -i ./req_traces/1_rdmdeltacom_high_load_short.txt -c 1 &' for http request traffic.

